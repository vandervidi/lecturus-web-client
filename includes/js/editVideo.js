var userEmail,
    userOrg,
    currentPos,
    moveTo,
    videoJson,
    coursesJson,
    rangeXzeroPosition,
    oneStep,
    rangeValue,
    tmpImg,
    tmpTag,
    moveElementToThisSecond,
    tagSec,
    tagPos,
    imageId,
    degreeSelectedPosition,
    courseSelectedPosition,
    degreeSelectedCode,
    courseSelectedCode,
    clickedTagText,
    currTagInitSecond,
    videoId,
    imageRef,
    imagesToDelete = [],
    appendImage,
    privacyStatus; // true = public, false = private


/* *********************************************************
 *                    Main function                 	   *
 * *********************************************************/
function initPage() {
	//Get user organization from local storage
	var userOrg = window.localStorage.getItem("userOrg");

	$(document).ready(function() {
		//initialize top nav in the html file
		injectTopNav();

		if (getParameterByName("videoId") != '') {
			videoId = getParameterByName("videoId");

			/*Get list of courses */
			$.ajax({
				type : "POST",
				url : 'http://lecturus.herokuapp.com/auxiliary/getCoursesByOrg',
				dataType : 'json',
				data : {
					org : userOrg
				},
				success : function(data) {
					coursesJson = data;
					// Get video json from lecturus web-service
					$.ajax({
						type : "POST",
						url : 'http://lecturus.herokuapp.com/session/getSessionById',
						data : {
							sessionId : videoId,
							org : userOrg,
							userId : userEmail,
							edit : true
						},
						dataType : 'json',
						success : function(data) {
							if (data.status == 1) {
								// Save this object for editing.
								videoJson = data.info;

								rangeXzeroPosition = $("#secondSlider").offset().left;
								//set max value for the time slider
								$("#secondSlider").attr("max", videoJson.totalSecondLength);
								oneStep = $("#secondSlider").width() / videoJson.totalSecondLength;
								rangeValue = parseInt($("#secondSlider").attr("value"));

								//set degrees list
								$.each(coursesJson.degrees, function(key, val) {
									if (val.id == videoJson.degreeId) {
										$("#listOfDegrees").append('<option value="' + key + '" selected>' + val.name + '</option>');
										degreeSelectedPosition = key;
										degreeSelectedCode = val.id;
									} else {
										$("#listOfDegrees").append('<option value="' + key + '">' + val.name + '</option>');
									}
								});
								//Degrees list change event
								$('#listOfDegrees').change(function() {
									courseSelectedPosition = 0;

									//Selected degree position in the array is:
									degreeSelectedPosition = $(this).val();

									//Update the VideoJson value for degree
									videoJson.degreeId = coursesJson.degrees[degreeSelectedPosition].id;
									videoJson.degree = coursesJson.degrees[degreeSelectedPosition].name;

									//update Courses list
									updateCoursesList(degreeSelectedPosition);

									//Clear  list
									$("#listOfLecturers").empty();

									//set the lecturer from the first course of the degree selected

									updateLecturersList(degreeSelectedPosition, courseSelectedPosition);
								});

								//set courses list

								$.each(coursesJson.degrees[degreeSelectedPosition].courses, function(key, val) {
									if (val.id == videoJson.courseId) {
										$("#listOfCourses").append('<option value="' + key + '" selected>' + val.name + '</option>');
										courseSelectedPosition = key;
										courseSelectedCode = val.id;
									} else {
										$("#listOfCourses").append('<option value="' + key + '">' + val.name + '</option>');
									}
								});

								//Courses list change event
								$('#listOfCourses').change(function() {

									//Selected course position in the array is:
									courseSelectedPosition = $(this).val();

									//save the lecturer of the first course in case the user doesnt choose it because it appears first in the  list
									videoJson.lecturer = coursesJson.degrees[degreeSelectedPosition].courses[courseSelectedPosition].lecturer;

									//Update the VideoJson value for course Id

									videoJson.courseId = coursesJson.degrees[degreeSelectedPosition].courses[courseSelectedPosition].id;
									videoJson.course = coursesJson.degrees[degreeSelectedPosition].courses[courseSelectedPosition].name;

									//Clear  list because we chose a new course
									$("#listOfLecturers").empty();

									//populate   list according to the chosen course
									updateLecturersList(degreeSelectedPosition, courseSelectedPosition);
								});

								//set lecturer list
								$.each(coursesJson.degrees[degreeSelectedPosition].courses[courseSelectedPosition].lecturers, function(key, val) {
									if (val == videoJson.lecturer) {
										$("#listOfLecturers").append('<option value="' + key + '" selected>' + val + '</option>');
									} else {
										$("#listOfLecturers").append('<option value="' + key + '">' + val + '</option>');
									}
								});

								//Lecturers list change event
								$('#listOfLecturers').change(function() {
									//Update the VideoJson value for lecturer
									videoJson.lecturer = $(this).val();

								});

								//Set title input
								$("#editLectureTitleInput").val(videoJson.title);

								//Set decription
								$("#descriptionLectureInput").val(videoJson.description);

								//Set privacy switch
								if (videoJson.public == true) {
									$("#privacyImg").css("background", "url('includes/img/edit_public.jpg') no-repeat left");
									 privacyStatus = true; // true = public, false = private

								} else if (videoJson.public == false) {
									$("#privacyImg").css("background", "url('includes/img/edit_private.jpg') no-repeat left");
									 privacyStatus = false; // true = public, false = private
								}

								$("#privacyImg").on("click", function() {
									
									if (privacyStatus) { // if this video is public, turn into privacy status -> private
										privacyStatus = false;
										$("#privacyImg").css("background", "url('includes/img/edit_private.jpg') no-repeat left");
										videoJson.public = false;
									} else {			//if this video is private, turn into privacy status -> public
										privacyStatus = true;
										$("#privacyImg").css("background", "url('includes/img/edit_public.jpg') no-repeat left");
										videoJson.public = true;
									}

								});

								//Create draggable images & create draggable tags
								$.each(videoJson.elements, function(key, val) {
									currTagInitSecond = key;

									if (val.photo) {
										$("<section>", {
											"html" : "<section><button class='deleteImg btn-xs' ><span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span></button></section><a href='" + val.photo.url + "' data-lightbox='image-" + key + "'><img src='" + val.photo.url + "'></a>",
											"class" : "draggableImage",
											"id" : "image" + val.photo.timestamp,
											"css" : {
												"left" : (oneStep * key) - 27
											}

										}).appendTo("#draggableImagesHolder");
									}
									if (val.tags) {
										$.each(val.tags, function(key, val) {
											$("<section>", {
												"id" : "tagPos" + key + "Sec" + currTagInitSecond,
												"class" : "draggableTag",
												"html" : "<section id='tagTop'></section><section id='editTagButtonsHolder'>" + "	<section id='tagOptionButton'>" + "		<button class='editButton'></button>" + "		<button  class='deleteButton'></button>" + " </section>" + "</section>" + "<section id='tagTextWrapper'><section id='tagTextHolder'>" + val.text + "</section> <section id='confirmOrCancelButtons'>" + "			<button id='confirmButton' class=''></button><button id='cancelButton' class=''></button>  </section>	</section>",
												"css" : {
													"left" : (oneStep * currTagInitSecond) - 72
												}
											}).appendTo("#draggableTagsHolder");
										});
									}
								});
								
								//Style for tags , when a mouse is focused on a tag
								$(".draggableTag").on("mouseover" ,function(){
									$(this).children("#tagTop").css(
										"background" , "url('includes/img/edit_hoverTagTop.png')"
									);
									
									$(this).children("#editTagButtonsHolder").children("#tagOptionButton").css(
										"background" , "#3c3c43"
									);
									
									$(this).children("#tagTextWrapper").css(
										"background" , "#3c3c43"
									);
									
									$(this).children("#editTagButtonsHolder").css({
										"border-left" : "2px solid #ff9a17",
										"border-right" : "2px solid #ff9a17"
									});
									
									$(this).children("#tagTextWrapper").children("#tagTextHolder").css(
										"border-left", "2px solid #27cbc5"
									);
									
									$(this).children("#editTagButtonsHolder").children("#tagOptionButton").children(".editButton").css(
										"background" , "url('includes/img/edit_editSmall.jpg') no-repeat"
									);
									
									$(this).children("#editTagButtonsHolder").children("#tagOptionButton").children(".deleteButton").css(
										"background" , "url('includes/img/edit_deleteSmall.jpg') no-repeat"
									);
									
									$(this).children("#tagTextWrapper").css({
										"border-left" : "2px solid #ff9a17",
										"border-right" : "2px solid #ff9a17",
										"border-bottom" : "2px solid #ff9a17"										
									});
								});
								
								//Style for tags , when a mouse is out of a tag
								$(".draggableTag").on("mouseout" ,function(){
									$(this).children("#tagTop").css(
										"background" , "url('includes/img/edit_tagTop.png')"
									);
									
									$(this).children("#editTagButtonsHolder").children("#tagOptionButton").css(
										"background" , "#29292e"
									);
									
									$(this).children("#tagTextWrapper").css(
										"background" , "#29292e"
									);
									
									$(this).children("#editTagButtonsHolder").css({
										"border-left" : "2px solid #000",
										"border-right" : "2px solid #000"
									});
									
									$(this).children("#tagTextWrapper").children("#tagTextHolder").css(
										"border-left" , "2px solid #1f8a86"
									);
									
									$(this).children("#editTagButtonsHolder").children("#tagOptionButton").children(".editButton").css(
										"background" , "url('includes/img/edit_editSmallGray.jpg') no-repeat"
									);
									
									$(this).children("#editTagButtonsHolder").children("#tagOptionButton").children(".deleteButton").css(
										"background" , "url('includes/img/edit_deleteSmallGray.jpg') no-repeat"
									);
									
									$(this).children("#tagTextWrapper").css({
										"border-left" : "2px solid #000",
										"border-right" : "2px solid #000",
										"border-bottom" : "2px solid #000"										
									});
								});
								
								
								//click event for deleting an image
								$(".deleteImg").on("click", function() {
									imageRef = $(this).closest(".draggableImage");
									imageId = imageRef.attr("id").split("image");
									//Set modal message text and option buttons
									//1. first clear its body
									$(".modal-body").empty();
									//2. prepare the text fro append
									appendString = "";
									appendString += "<p>" + " Deleting this image will delete it forever without an option to restore it." + " </p> " + " <p> " + " <button id='confirmDeletetion' type='button' class='btn btn-danger'> " + " 	Ok, Delete " + " </button> " + " <button type='button' class='btn btn-default' data-dismiss='modal'> " + " 	Cancel " + " </button> " + " </p>";
									//3. append body content
									$(".modal-body").append(appendString);
									//4. attach event listeners to option buttons
									//click event for delete confirmation button (Modal)
									$("#confirmDeletetion").on("click", function() {
										imageRef.hide();
										$("#deleteConfirmation").modal("hide");
										imagesToDelete.push(videoJson.elements[imageId[1]].photo.url);
										delete videoJson.elements[imageId[1]].photo;
										if ( typeof videoJson.elements[imageId[1]].photo == "undefined") {
											delete videoJson.elements[imageId[1]];
										}
									});
									$("#deleteConfirmation").modal("show");
								});

								//calculate slider position when dragging images and tags
								//update slider position
								// make sure dragged element doesnt return a position that is out of bounds
								$(".draggableImage, .draggableTag").draggable({
									axis : "x", //set movement only on x axis
									containment : "#editWrapper", //The parent element that contains the draggable elements
									drag : function(event, ui) {
										stopPlayer();
										currentPos = $(this).position();
										moveTo = (currentPos.left + ($(this).width() / 2)) * (videoJson.totalSecondLength / $("#secondSlider").width());
										if (moveTo < 0)
											moveTo = 0;
										if (moveTo > videoJson.totalSecondLength) {
											moveTo = videoJson.totalSecondLength;
										}
										// make sure the slider doesnt drag objects to negative values
										$("#secondSlider").val(moveTo);
										$("#secondSlider").attr("value", moveTo);
									}
								});

								//setting a click event for a draggable tag.
								//update slider position
								// make sure clicked element doesnt return a position that is out of bounds
								$(".draggableTag,.draggableImage").on("mousedown", (function() {
									stopPlayer();
									currentPos = $(this).position();
									moveTo = (currentPos.left + ($(this).width() / 2)) * (videoJson.totalSecondLength / $("#secondSlider").width());
									if (moveTo < 0)
										moveTo = 0;
									if (moveTo > videoJson.totalSecondLength) {
										moveTo = videoJson.totalSecondLength;
									}
									// make sure the slider doesnt drag objects to negative values
									$("#secondSlider").val(moveTo);
									$("#secondSlider").attr("value", moveTo);

								}));

								//Draggable elements mouseup event handler
								$(".draggableImage, .draggableTag").on("mouseup", (function() {
									//Get the value of the slider when an object is released after beeing dragged
									moveElementToThisSecond = parseInt($("#secondSlider").attr("value"), 10);

									//In case a image is dragged
									if ($(this).hasClass("draggableImage")) {
										//Get the image id of dragged image
										imageId = $(this).attr("id").split("image");

										//Save its value from the Json file representing a video
										tmpImg = videoJson.elements[imageId[1]].photo;

										//modify dragged image timestamps (required to be a string)
										tmpImg.timestamp = "" + moveElementToThisSecond;

										//delete the image from the Json - it's old position
										delete videoJson.elements[imageId[1]].photo;

										if ( typeof videoJson.elements[imageId[1]].tags == "undefined") {
											delete videoJson.elements[imageId[1]];
										}

										//Set a new 'id' attribute to this element
										$(this).attr("id", "image" + moveElementToThisSecond);

										//Add the image to the Json in its new position according to the seconds
										//case 1: Current second has also a tag
										if ( typeof videoJson.elements[moveElementToThisSecond] != "undefined") {
											//	videoJson.elements[moveElementToThisSecond]['photo'] = tmpImg;
											videoJson.elements[moveElementToThisSecond].photo = tmpImg;

											//case 3 : the element is empty. Add the photo
										} else {
											videoJson.elements[moveElementToThisSecond] = {
												"photo" : tmpImg
											};
										}
									}

									//In case a tag is dragged
									else if ($(this).hasClass("draggableTag")) {
										$(".draggableTag button").unbind("mouseup");

										tagSec = $(this).attr("id").split("Sec");
										tagPos = tagSec[0].split("Pos");

										//Save its value from the Json file representing a tag
										tmpTag = videoJson.elements[tagSec[1]].tags[tagPos[1]];

										//modify the dragged tag timestamp
										tmpTag.timestamp = "" + moveElementToThisSecond;

										//delete the tag from the Json
										//delete videoJson.elements[tagSec[1]].tags[tagPos[1]];
										videoJson.elements[tagSec[1]].tags.splice(parseInt([tagPos[1]], 10), 1);

										//If 'Tags' array is empty then delete 'Tags' from the videoJson file
										if (videoJson.elements[tagSec[1]].tags.length == 0) {
											delete videoJson.elements[tagSec[1]].tags;
										}

										//if 'image' and 'Tags array are empty , Delete the whole element from videoJson
										if (( typeof videoJson.elements[tagSec[1]].tags == "undefined") && ( typeof videoJson.elements[tagSec[1]].photo == "undefined")) {
											delete videoJson.elements[tagSec[1]];
										}

										//Add the tag to the Json in its new position according to the seconds
										//case 1: Current second already has some tags
										if ( typeof videoJson.elements[moveElementToThisSecond] != "undefined") {
											if ( typeof videoJson.elements[moveElementToThisSecond].tags != "undefined") {
												videoJson.elements[moveElementToThisSecond].tags.push(tmpTag);

											} else {
												//case 2 : the element doesnt have tags at all
												//			but has an photo
												if ( typeof videoJson.elements[moveElementToThisSecond].photo != "undefined") {
													//save the photo
													tmpImg = videoJson.elements[moveElementToThisSecond].photo;
													videoJson.elements[moveElementToThisSecond] = {
														"tags" : [tmpTag],
														"photo" : [tmpImg]
													};
												}

											}
										} else {
											videoJson.elements[moveElementToThisSecond] = {
												"tags" : [tmpTag]
											};
										}

										//Set a new 'id' attribute to this element
										$(this).attr("id", "tagPos" + (videoJson.elements[moveElementToThisSecond].tags.length - 1) + "Sec" + moveElementToThisSecond);

									}
									$("#secondSlider").trigger('input');
									$("#secondSlider").trigger('change');
								}));

								//edit a tag button click listener
								$(".draggableTag .editButton").on("click", function() {
									//pause the player
									togglePlayPause();
									//Display confirm or cancel buttons
									var aaa = $(this).parents().eq(2).children("#tagTextWrapper").children("#confirmOrCancelButtons").css("display", "block");
									
									
									//Save tag text before editing
									var refThis = $(this).parents().eq(2).children("#tagTextWrapper").children("#tagTextHolder");

									//Save the text currently displayed
									clickedTagText = refThis.text();

									//clear the tag text and append a text area
									refThis.empty();
									var inputForTag = "<textarea id='editTagTextArea' rows='4' cols='20'>" + clickedTagText + "</textarea>";
									refThis.append(inputForTag);

									//Make sure that the player is paused when typing in the text area
									$("#editTagTextArea").on("click change", function() {
										//pause the player
										togglePlayPause();
									});

								});

								//confirm tag edit button click listener
								$(".draggableTag #confirmButton").on("click", function() {
									//reference to parent
									var refThis = $(this).parents().eq(1);
									//Get text from the text area
									var textAreaTagText = refThis.children("#tagTextHolder").children("#editTagTextArea").val();
									
									//empty 'tagTextHolder'
									refThis.children("#tagTextHolder").empty();

									//Hide confirm or cancel buttons
									$(this).parents().children("#confirmOrCancelButtons").css("display", "none");

									//save new tag to videoJson Json
									var editedTagSec = $(this).parents().eq(2).attr("id").split("Sec");
									var editedTagPos = editedTagSec[0].split("Pos");
									videoJson.elements[editedTagSec[1]].tags[editedTagPos[1]].text = "" + textAreaTagText;

									//add text of new tag to tagTextHolder section
									refThis.children("#tagTextHolder").text(textAreaTagText);
								});

								//Cancel a tag edit button click listener
								$(".draggableTag #cancelButton").on("click", function() {
									var refThis = $(this).parents().eq(1).children("#tagTextHolder");

									//Save the text currently displayed
									var clickedTagText = refThis.text();

									//Hide confirm or cancel buttons
									$(this).parents().children("#confirmOrCancelButtons").css("display", "none");

									//empty 'tagTextHolder'
									refThis.empty();

									//add text of old tag to tagTextHolder section
									refThis.text(clickedTagText);
								});

								//Deleta a tag button click listener
								$(".draggableTag .deleteButton").on("click", function() {
									//delete the tag from the Json
									var refThis = $(this).parents().eq(2);
									tagSec = $(this).parents().eq(2).attr("id").split("Sec");
									tagPos = tagSec[0].split("Pos");
									videoJson.elements[tagSec[1]].tags.splice(tagPos[1], 1);

									//Set modal message text and option buttons
									//1. first clear its body
									$(".modal-body").empty();
									//2. prepare the text fro append
									appendString = "";
									appendString += "<p>" + " Deleting this tag will delete it forever without an option to restore it." + " </p> " + " <p> " + " <button id='confirmDeletetion' type='button' class='btn btn-danger'> " + " 	Ok, Delete " + " </button> " + " <button type='button' class='btn btn-default' data-dismiss='modal'> " + " 	Cancel " + " </button> " + " </p>";
									//3. append body content
									$(".modal-body").append(appendString);
									//4. attach event listeners to option buttons
									//click event for delete confirmation button (Modal)
									$("#confirmDeletetion").on("click", function() {
										$("#deleteConfirmation").modal("hide");
										if ((videoJson.elements[tagSec[1]].tags.length == 0) && ( typeof videoJson.elements[tagSec[1]].photo == "undefined")) {
											delete videoJson.elements[tagSec[1]];

										} else if ((videoJson.elements[tagSec[1]].tags.length == 0)) {
											delete videoJson.elements[tagSec[1]].tags;
										}

										//after deletion remove this section from the DOM
										refThis.remove();
									});
									$("#deleteConfirmation").modal("show");

								});

								//save changes click listener
								$("#saveChanges").on("click", function() {
									//save text from 'title' input to the json
									videoJson.title = $("#editLectureTitleInput").val();

									//save text from description text area to the json
									videoJson.description = $("#descriptionLectureInput").val();

									$.ajax({
										type : "POST",
										url : 'http://lecturus.herokuapp.com/session/updateSession/',
										dataType : 'json',
										contentType : "application/json",
										data : JSON.stringify(videoJson),
										success : function(data) {
											if (data.status == 1) {
												
												//delete images from the server if the user deleted any.
												$.each(imagesToDelete, function(key, val) {
													$.ajax({
														type : "POST",
														url : "http://lecturus.herokuapp.com/session/deleteImage",
														data : {
															imageurl : val
														},
														success : function(data) {
														},
														error : function(objRequest, errortype) {
															console.log("Something went wrong... we couldnt delete  your images from the cloud");
														}
													});
												});

												window.location.replace(document.referrer);
											}

										},
										error : function(objRequest, errortype) {
											console.log("Something went wrong... we couldnt save your changes");
										}
									});

								});

								//Go back button click listener
								$("#goBack").on("click", function() {
									window.location.href = document.referrer;
								});

								//Delete this session button click listener
								$("#deleteSession").on("click", function() {
									//Set modal message text and option buttons
									//1. first clear its body
									$(".modal-body").empty();
									
									//2. prepare the text fro append
									appendString = "";
									appendString += "<p>" + " Deleting this lecture will delete it forever without an option to restore it." + " </p> " + " <p> " + " <button id='confirmDeletetion' type='button' class='btn btn-danger'> " + " 	Ok, Delete " + " </button> " + " <button type='button' class='btn btn-default' data-dismiss='modal'> " + " 	Cancel " + " </button> " + " </p>";
									
									//3. append body content
									$(".modal-body").append(appendString);
									
									//4. attach event listeners to option buttons
									//click event for delete confirmation button (Modal)
									$("#confirmDeletetion").on("click", function() {
										$("#deleteConfirmation").modal("hide");
										//delete this session from the servers
											$.ajax({
												type : "POST",
												url : 'http://lecturus.herokuapp.com/session/deleteSession',
												dataType : 'json',
												
												data : {
													sessionId : videoJson.sessionId,
													userId : userEmail
												},
												success : function(data) {
													if(data.status == 1)
														window.location.replace("home.html");
													},
												error : function(objRequest, errortype) {
													console.log("Something went wrong... we couldnt delete this lecture");
													}
											});
									});
									$("#deleteConfirmation").modal("show");

								});

								// // Media-player
								run_media_player(false);
								
							} else {
								console.log('server status code is not 1');
							}
						},
						error : function(objRequest, errortype) {
							console.log("Cannot get video Json");
						}
					});

				},
				error : function(objRequest, errortype) {
				}
			});
		} else {
			//redirect to home page if this parameter is missing
			window.location.href = "home.html";
		}
	});
}

/************************************************* 
 * A function that recieves a degree Id number   *
 * and updates the courses list accordingly      *
 ************************************************/
function updateCoursesList(degreeSelectedPosition) {
	//Empty the list first
	$("#listOfCourses").empty();

	//set courses list
	$.each(coursesJson.degrees[degreeSelectedPosition].courses, function(key, val) {
		//set the first course as the chosen one in case the user doesnt choose any
		if (key == 0) {
			videoJson.courseId = val.id;
			videoJson.course = val.name;
		}
		$("#listOfCourses").append('<option value="' + key + '">' + val.name + '</option>');
	});
}

/******************************************************
 *  This functions updates the lecturers names		  *
 *  according to the selection in the Degree and      *
 *  Course inputs                                     *
 *****************************************************/
function updateLecturersList(degreeSelectedPosition, courseSelectedPosition) {
	//Empty the list first
	$("#listOfLecturers").empty();

	//set lecturers list
	$.each(coursesJson.degrees[degreeSelectedPosition].courses[courseSelectedPosition].lecturers, function(key, val) {
		//set the first lecturer as the chosen one in case the user doesnt choose any
		if (key == 0) {
			videoJson.lecturer = val;
		}
		$("#listOfLecturers").append('<option value="' + key + '">' + val + '</option>');

	});
}

/****************************************************
 * A function that recieves a google callback json  *
 * and returns user's email                         *
****************************************************/
function getPrimaryEmail(resp) {
	for (var i = 0; i < resp.emails.length; i++) {
		if (resp.emails[i].type === 'account')
			return resp.emails[i].value;
	}

}