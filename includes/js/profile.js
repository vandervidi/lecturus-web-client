var user,
    jsonRef;


/* *********************************************************
 *                    Main function                 	   *
 * ********************************************************/
function initPage() {
	//inject top navigation bar
	injectTopNav();

	// get user parameter from the query string
	if (getParameterByName("user") != '') {
		user = getParameterByName("user");
	}

	//get user data
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/users/getUsersData',
		dataType : 'json',
		data : {
			users : [user]
		},
		success : function(data) {
			if (data.status == 1) {
				//append profile picture
				$("#userProfilePic").append("<img class='profilePic' src='" + data.users[0].image + "'>");

				//append name and last name
				$("#name").append(data.users[0].name + " " + data.users[0].lastName);

				//Hide loading spinner
				$(".loading").hide();
			}
		},
		error : function(objRequest, errortype) {
			console.log("Cannot get video Json");
		}
	});

	//get user's sessions from the server
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/session/getUserSessions',
		data : {
			email : user,
			from : 0,
			to : 4
		},
		success : function(data) {
			if (data.status == 1) {
				$("#urTitle").append("Latest recordings:");
				jsonRef = data;
				if (data.userRecordings.length == 0) {
					appendString = "Recordings list is empty";
				} else {
					//Prepare data and inject it to the DOM
					createTemplateAndAppendVideos(data.userRecordings);
				}
			}

		},
		error : function(objRequest, errortype) {
			console.log("Cannot get my sessions Json");
		}
	});

	//get followers
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/auxiliary/followedUsers',
		dataType : 'json',
		data : {
			email : user
		},
		success : function(data) {
			if (data.status == 1) {
				var appendString = '';
				//append title
				$("#fwTitle").append("Followed users:");

				//append profile picture of followed users
				if (Object.keys(data.res).length == 0) {
					appendString = "Following list is empty";
				} else {
					$.each(data.res, function(key, val) {
						if (key != user) {
							appendString += "<a href='profile.html?user=" + key + "'><img class='profilePic' src='" + data.users[key].image + "' title='" + data.users[key].name + " " + data.users[key].lastName + "'></a>";
						}
					});
				}
				$("#followedUsers").append(appendString);
				$("#followedUsers .profilePic").mouseenter(function() {
					$(this).animate({
						opacity : 0.5
					}, 200);
				}).mouseleave(function() {
					$(this).animate({

						opacity : 1
					}, 200);
				});

				//Hide loading spinner
				$(".loading").hide();
			}
		},
		error : function(objRequest, errortype) {
			console.log("Cannot get video Json");
		}
	});
}

/************************************************************
 * This function recieves an ARRAY of videos and creates a  *
 * HTML template whic is finally being append to the DOM    *
 ***********************************************************/
function createTemplateAndAppendVideos(arrayOfVideos) {

	appendString = "";
	var counter = -1;
	//first clear Video Wrapper from any videos and then append new videos
	$("#videosWrapper").empty();

	$.each(arrayOfVideos, function(key, val) {
		counter++;
		//Start new row of videos
		if ((counter % 4) == 0) {

			appendString += "<section class='row'> " + " <section class='col-md-3'>" + "<section class='singleVideoWrapper'>" + "<section class='videoImage'>" + "<a href='playmovie.html?videoId=" + val.sessionId + "'><img src='" + thumbnails[val.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";

	
			appendString += "<section class='videoTitle'><a href='playmovie.html?videoId=" + val.sessionId + "'>" + val.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val.degree + " , " + val.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val.lecturer + "</section></section>" 
			+ "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val.views + " </section>";
					//populate participants section
			//Add current video owner profile picture as a prticipant
			appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val.owner].image + "' title='" + jsonRef.users[val.owner].name + " " + jsonRef.users[val.owner].lastName + "' /></a>";

			//Add other particimapnts profile pictures
			$.each(val.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
			});
			appendString += "</section>"+ "	  </section>" + " </section>";

		}
		//otherwise append to the existong row of videos
		else {
			appendString += " <section class='col-md-3'>" + "	<section class='singleVideoWrapper'>" + " <section class='videoImage'>" + "<a href='playmovie.html?videoId=" + val.sessionId + "'><img src='" + thumbnails[val.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";
			appendString += "<section class='videoTitle'><a href='playmovie.html?videoId=" + val.sessionId + "'>" + val.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val.degree + " , " + val.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val.lecturer + "</section></section>" + "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val.views + "</section>"; 
			//populate participants section
			//Add current video owner profile picture as a prticipant
			appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val.owner].image + "' title='" + jsonRef.users[val.owner].name + " " + jsonRef.users[val.owner].lastName + "' /></a>";

			//Add other particimapnts profile pictures
			$.each(val.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
			});
			appendString += "</section>"+ "	  </section>" + " </section>";

			//if the next item opens a new row, Close current  row section
			if ((counter + 1) % 4 == 0) {
				appendString += "</section>";
			}

		}
	});

	//Close last row at the end
	if ((counter + 1) % 4 != 0) {
		appendString += "</section>";

	}

	//finally, append  everything
	$("#videosWrapper").append(appendString);

}