var userEmail,
    appendString,
    loopingKey,
    from = 0,
    to = 20,
    getMoreLectures = false,
    jsonRef;

// Configuring infinite scroll. When a desired scroll height is reached, call a function to get more videos.
$(window).scroll(function() {
	if (($(window).scrollTop() + $(window).height() > $(document).height() - 100) && getMoreLectures) {
		$("#loadingMoreMessage").show();
		loadMore();
	}
});

/* *********************************************************
 *                    Main function                 	   *
 * ********************************************************/
function initPage() {
	getMoreLectures = true;
	//initialize top nav in the html file
	injectTopNav();

	$(document).ready(function() {
		//Check to see if the window is top if not then display button
		$(window).scroll(function() {
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});

		//Click event to scroll to top
		$('.scrollToTop').click(function() {
			$('html, body').animate({
				scrollTop : 0
			}, 800);
			return false;
		});

		//get user's sessions from the server
		$.ajax({
			type : "POST",
			url : 'http://lecturus.herokuapp.com/session/getUserSessions',
			data : {
				email : userEmail,
				from : from,
				to : to
			},
			success : function(data) {
				if (data.status == 1) {
					if (data.userRecordings.length < 20) {
						getMoreLectures = false;
					}
					jsonRef = data;
					//Prepare data and inject it to the DOM
					createTemplateAndAppendVideos(data.userRecordings);
				}
			},
			error : function(objRequest, errortype) {
				console.log("Cannot get my sessions Json");
			}
		});
	});
}


/* ************************************************************
 * This function ןis called during an infinite scroll feature  *
 * and is responsible for retrieveing a new chunk of videos   *
 * ************************************************************
 */

function loadMore() {
	from = to;
	to += 20;

	$.ajax({
		type : "POST",
		url : "http://lecturus.herokuapp.com/session/getUserSessions",
		dataType : "json",
		data : {
			email : userEmail,
			from : from,
			to : to
		},
		success : function(data) {
			if (data.status == 1) {
				if (data.userRecordings.length < 20) {
					getMoreLectures = false;
				}
				jsonRef = data;
				createTemplateAndAppendVideos(data.userRecordings);
				$("#loadingMoreMessage").hide();
			}
		},
		error : function(objRequest, errortype) {
			console.log("Cannot get ym recordings Json");
		}
	});
}

/************************************************************
 * This function recieves an ARRAY of videos and creates a  *
 * HTML template which is finally being append to the DOM   *
 ***********************************************************/
function createTemplateAndAppendVideos(arrayOfVideos) {
	appendString = "";
	var counter = -1;
	$(".loading").hide();
	$.each(arrayOfVideos, function(key, val) {
		counter++;
		//Start new row of videos
		if ((counter % 4) == 0) {

			appendString += "<section class='row'> " + " <section class='col-md-3'>" + "<section class='singleVideoWrapper'>" + "<section class='videoImage'><a href='editVideo.html?videoId=" + val.sessionId + "'><button type='button' class='editVideoButton'></button></a>" + "<a href='playmovie.html?videoId=" + val.sessionId + "'><img src='" + thumbnails[val.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";
			appendString += "<section class='videoTitle'><a href='playmovie.html?videoId=" + val.sessionId + "'>" + val.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val.degree + " , " + val.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val.lecturer + "</section></section>" 
			+ "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val.views + " </section>";
					//populate participants section
			//Add current video owner profile picture as a prticipant
			appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val.owner].image + "' title='" + jsonRef.users[val.owner].name + " " + jsonRef.users[val.owner].lastName + "' /></a>";

			//Add other particimapnts profile pictures
			$.each(val.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
			});
			appendString += "</section>"+ "	  </section>" + " </section>";

		}
		//otherwise append to the existong row of videos
		else {
			appendString += " <section class='col-md-3'>" + "	<section class='singleVideoWrapper'>" + " <section class='videoImage'><a href='editVideo.html?videoId=" + val.sessionId + "'><button type='button' class='editVideoButton'></button></a>" + "<a href='playmovie.html?videoId=" + val.sessionId + "'><img src='" + thumbnails[val.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";
			appendString += "<section class='videoTitle'><a href='playmovie.html?videoId=" + val.sessionId + "'>" + val.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val.degree + " , " + val.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val.lecturer + "</section></section>" + "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val.views + "</section>"; 
			//populate participants section
			//Add current video owner profile picture as a prticipant
			appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val.owner].image + "' title='" + jsonRef.users[val.owner].name + " " + jsonRef.users[val.owner].lastName + "' /></a>";

			//Add other particimapnts profile pictures
			$.each(val.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
			});
			appendString += "</section>"+ "	  </section>" + " </section>";

			//if the next item opens a new row, Close current  row section
			if ((counter + 1) % 4 == 0) {
				appendString += "</section>";
			}

		}
	});

	//Close last row at the end
	if ((counter + 1) % 4 != 0) {
		appendString += "</section>";

	}

	//finally, append  everything
	$("#videosWrapper").append(appendString);

}