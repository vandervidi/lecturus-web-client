var fullMode = true;

// Json get from google+
var googleResp;
var userEmail;

// Json get from lecturus
var videoJson;

// Sound ref
var mediaSound;

// Whole video second counter
var video_timeCounter = 0;

// Specific audio playing
var audio_timeCounter = 0;

// Stopwatch for interval
var starttimeMS;
var endtimeMS;
var totaltimeMS = 0;
var resttimeMS = 0;
var interval;

// Flags to handle play/pause auto in seeking & self-finish
var seek = false;
var currentEnd_loadSecond = false;

// Player buttons
var playPauseBtn;
var muteBtn;

// Song contains the second user seek to
var bestMatch;

var op = {
	height : 256,
	width : 256
};

var viewsWasIncrement = false;

var default_poster = null;

$( document ).ready(function() {
  	if (window.location.pathname.indexOf('editVideo.html') >= 0)
		fullMode = false;
});


function initPage(){
	//inject top navigation bar
	injectTopNav();
	
	if (fullMode) {
		console.log('full mode');

		/* download video json from lecturus web-service */
		var videoId = "";
		if (getParameterByName("videoId") != '') {
			videoId = getParameterByName("videoId");

			var userOrg = window.localStorage.getItem("userOrg");
			if (userOrg != null) {
				$.ajax({
					type : "Post",
					url : 'http://lecturus.herokuapp.com/session/getSessionById', //includes/js/example_video.json',
					dataType : 'json',	//ContentType : 'application/x-www-form-urlencoded',
					data : {
						sessionId : videoId,
						userId : userEmail,
						org : userOrg
					},
					success : function(data) {
						//debugger;
						if (data.status == 1) {
							// Build video obj
							videoJson = data.info;
							
							default_poster = thumbnails[videoJson.degreeId].xl;
							$('#viewPhotos').attr('src', default_poster);

							// Check if admin
							show_edit_button_if_admin_or_participants();

							run_media_player(true);
						} else {
							console.log('server status code are not 1. ' +data.desc);
						}
					},
					error : function(objRequest, errortype) {
						console.log(errortype);
						console.log("Can't do because: " + errortype);
					}
				});
			} else {
				console.error('userOrg is null');
			}

		} else {
			console.error('QueryString "?videoId=NUMBER" are missing!');
		}
	}
}		
function addReminderInTopOfRange(elementType, value, sec){
	//calculate length of a second in the slider
	var secondLength = $('#remindersHolder').width() / videoJson.totalSecondLength;
		
	var section = $('<section>');
	
	switch (elementType){
	case 'tag':
		section.addClass('glyphicon-comment');
		section.attr('title', value.email);
		section.attr('data-sec',sec);
		section.attr('data-id',value.id);
		break;
	case 'tags':
		section.addClass('glyphicon-comment');
		
		//Get all tags in this second
		allTagsMailsInSecond = [];
		allTagsIdsInSecond = [];
		$.each(value.tags, function(i, tagJson) {
			allTagsMailsInSecond.push(tagJson.email);
			allTagsIdsInSecond.push(tagJson.id);
		});
		section.attr('title', allTagsMailsInSecond);
		section.attr('data-sec',sec);
		section.attr('data-id',allTagsIdsInSecond);
		break;
	case 'photo':
		section.addClass('glyphicon-picture');
		section.attr('title', value.photo.email);
		break;
	}
	
	section.attr('onclick','elementClick('+sec+')');
			
	section.addClass('glyphicon');
	section.addClass('sliderReminderIcon');
	section.addClass('rangeElement'+sec);
	var remindersHolderLeftVal = $('#remindersHolder').position().left;
	section.css('left', (remindersHolderLeftVal + parseInt(sec)*secondLength) + 'px');
	section.css('top',  $('#remindersHolder').position().top+5 + 'px');

	// append child to parent
	$('#remindersHolder').append(section);
}
function run_media_player(fullMode) {
	// init media obj
	mediaSound = $('#mediaSound')[0];
	//debugger;
	initialiseMediaPlayer();
	
	if (fullMode) {
		// set the title
		// $("#degreeName").html(videoJson.degree);
		// $('#courseName').html('&nbsp;> ' + videoJson.course);
		// $('#videoTitle').html('&nbsp;> ' + videoJson.title);
		
		$('#userPic').parent()[0].href = 'profile.html?user='+googleResp.emails[0].value;
		$('#userPic').attr('src', googleResp.image.url);
		
		// set all tags/images 'reminders' in the top of slider
		$.each(videoJson.elements, function(sec, value) {
			var element = null;
			if (value.hasOwnProperty('tags')) {
				element = 'tags';
			} else if (value.hasOwnProperty('photo')) {
				element = 'photo';
			}
			
			if (element != null){
				addReminderInTopOfRange(element, value, sec);
			}
		});
		
		// Set fulltime
		$('#fullTime').html('&nbsp;&nbsp;/&nbsp;&nbsp;'+secondToTime(videoJson.totalSecondLength));

		$('#title').text(videoJson.title);
		
		$('#profileBody').parent()[0].href = 'profile.html?user='+videoJson.owner;
		$('#profileBody').attr("src", 'includes/img/personThumbnail.jpg');
		$('#profileBody').attr('title', videoJson.owner);
		
		if (userEmail != videoJson.owner){
			if (videoJson.follow) {
				$('#followText').text('Unfollow');
			} else {
				$('followText').text('Follow');
			}
		}
		
		if (videoJson.favorite) {
			$('#favoritesButtonSpan').addClass('favoritesButtonClicked');
		}

		// Set owner & participants
		set_owner_profile(videoJson.owner);
		if (videoJson.participants.length > 0){
			$.each(videoJson.participants, function(i, email) {
				userDetails = videoJson.users[email];
				var iconTag;
				if (userDetails != null) {
					iconTag = '<img title="' + userDetails.name + ' ' + userDetails.lastName + '" data-email="' + email + '" class="profilePic profileBodyMini tooltip" src=' + userDetails.image + '></img>';
				} else {
					iconTag = '<img title="' + email + '" class="profilePic profileBodyMini tooltip" src="includes/img/personThumbnail.jpg"></img>';
				}
				
				var iconTagLink = $('<a>');
				iconTagLink.attr('href', 'profile.html?user='+email);
				iconTagLink.append(iconTag);
				
				$('#participants').append(iconTagLink);
			});
		}else{
			$('#videoParticipants').remove();
		}
		

		$('#viewTimesText').text(videoJson.views);

		// Votes
		$('#movieUnlikeVal').text(videoJson.rating.negative.value);
		$('#movieLikeVal').text(videoJson.rating.positive.value);
		if (videoJson.rating.negative.users) {
			//set green opacity
			$('#movieLikeButton').addClass('opacityButton');
			console.log('user has unlike this video');

		} else if (videoJson.rating.positive.users) {
			//set red opacity
			$('#movieUnlikeButton').addClass('opacityButton');
			console.log('user has like this video');
		}

		$('#share').on('click', function(e) {
			if ($('#shareIcons').children().length > 0) {
				$('#share').removeClass('btn-warning');
				$('#shareIcons').empty();
			} else {
				$('#share').addClass('btn-warning');
				$('#shareIcons').share({
					networks : ['facebook', 'googleplus', 'email', 'twitter', 'linkedin', 'pinterest', 'tumblr', 'in1', 'stumbleupon', 'digg'],
					theme : 'square'
				});
			}
		});
		$('#date').text(dataFromTimestamp(videoJson.stopTime).fullDate);
		// $('#organization').text('- at ' + videoJson.org);
		$('#description').text(videoJson.description);
		
		
		// init tags section
		init_tags_section();
		
	}
	//put the first audio
	if (videoJson.audios.length > 0) {

		$("#audioSrc").attr("src", videoJson.audios[audio_timeCounter].url);
		mediaSound.load();

		//adjust slider for full video length
		$("#secondSlider").prop({
			'min' : 0,
			'max' : videoJson.totalSecondLength
		});

		// slider events handler
		secondSlider_Handler();
	} else {
		console.error('Error: there is not sound in the json!');
	}
}
function appendNewTagToTags(tag, sec, manual){
	var tagsContainer = $('#tagsContainer');
	
	var itemTag = $('<section id="' + tag.id + '" >');
	
	var iconTagLink = $('<a>');
	iconTagLink.attr('href', 'profile.html?user='+tag.email);
	// Set icon tag
	var iconTag = '<img title="' + videoJson.users[tag.email].name + ' ' + videoJson.users[tag.email].lastName + '" data-email="' + tag.email +'" class="profilePic profileBodyMini tagsProfilePic tooltip" src=' + videoJson.users[tag.email].image + '></img>';			//iconTag = '<img title="' + item + '" class="profilePic profileBodyMini tooltip" src="includes/img/personThumbnail.jpg"></img>';
	// append icon hirrarchy
	iconTagLink.append(iconTag);
	
	// Set all data
	var allTagData = $('<section>');
	
	// Set Name tag
	var nameTag = '<section class="tagFullName">'+videoJson.users[tag.email].name + ' ' + videoJson.users[tag.email].lastName+'</section>';
	// Set text tag	
	var textTag = $('<section class="brightText">');
	textTag.text(tag.text);
	
	allTagData.append(nameTag);
	allTagData.append(textTag);
	
	// Set buttons tags
	if (tag.rating.positive.users) {
		var buttonsTag = (
			"<section class='voteContainerLine'>"
			+"	<section class='voteContainer'>"
			+"		<button type='button' id='' class='btn btn-xs likeAndUnlikeButton likeButton' title='i like this' aria-label='Left Align' onclick='set_user_vote(this, true);'>"
			+"			<span class='glyphicon glyphicon-thumbs-up leftIcon' aria-hidden='true'></span><span id='tagLikeVal' class='likeVal'>" + tag.rating.positive.value+ "</span>"
			+"		</button>"
			+"		<button type='button' id='' class='btn btn-xs likeAndUnlikeButton unlikeButton opacityButton' title='i dont like this' aria-label='Left Align' onclick='set_user_vote(this, false);'>"
			+"			<span class='glyphicon glyphicon-thumbs-down leftIcon' aria-hidden='true'></span><span id='tagUnlikeVal' class='unlikeVal'>" + tag.rating.negative.value + "</span>"
			+"		</button>"
			+"		<div class='clear'></div>"
			+"	</section>"
			+"</section>"
		);
	}else if (tag.rating.negative.users) {
		var buttonsTag = (
			"<section class='voteContainerLine'>"
			+"	<section class='voteContainer'>"
			+"		<button type='button' id='' class='btn btn-xs likeAndUnlikeButton likeButton opacityButton' title='i like this' aria-label='Left Align' onclick='set_user_vote(this, true);'>"
			+"			<span class='glyphicon glyphicon-thumbs-up leftIcon' aria-hidden='true'></span><span id='tagLikeVal' class='likeVal'>" + tag.rating.positive.value+ "</span>"
			+"		</button>"
			+"		<button type='button' id='' class='btn btn-xs likeAndUnlikeButton unlikeButton' title='i dont like this' aria-label='Left Align' onclick='set_user_vote(this, false);'>"
			+"			<span class='glyphicon glyphicon-thumbs-down leftIcon' aria-hidden='true'></span><span id='tagUnlikeVal' class='unlikeVal'>" + tag.rating.negative.value + "</span>"
			+"		</button>"
			+"		<div class='clear'></div>"
			+"	</section>"
			+"</section>"
		);
	}else {
		var buttonsTag = (
			"<section class='voteContainerLine'>"
			+"	<section class='voteContainer'>"
			+"		<button type='button' id='' class='btn btn-xs likeAndUnlikeButton likeButton' title='i like this' aria-label='Left Align' onclick='set_user_vote(this, true);'>"
			+"			<span class='glyphicon glyphicon-thumbs-up leftIcon' aria-hidden='true'></span><span id='tagLikeVal' class='likeVal'>" + tag.rating.positive.value+ "</span>"
			+"		</button>"
			+"		<button type='button' id='' class='btn btn-xs likeAndUnlikeButton unlikeButton' title='i dont like this' aria-label='Left Align' onclick='set_user_vote(this, false);'>"
			+"			<span class='glyphicon glyphicon-thumbs-down leftIcon' aria-hidden='true'></span><span id='tagUnlikeVal' class='unlikeVal'>" + tag.rating.negative.value + "</span>"
			+"		</button>"
			+"		<div class='clear'></div>"
			+"	</section>"
			+"</section>"
		);
	}
	
	// set class (second & bubble/bubble2)
	itemTag.addClass(String(sec));
	itemTag.attr('data-sec',sec);
	itemTag.attr('data-id',tag.id);
	if (tag.email == userEmail){
		itemTag.addClass('bubble2');
		
		// Add remove button
		var removeButton = "<section class='glyphicon glyphicon-remove removeButton' aria-hidden='true' onclick='removeTag(this);'></section>";
		itemTag.append(removeButton);
		
	}else itemTag.addClass('bubble');
	
	// Set onClick function
	itemTag.attr('ondblclick','elementClick('+sec+')');
	
	// Add tags hirrarchi
	itemTag.append(iconTagLink);
	itemTag.append(allTagData);
	itemTag.append(buttonsTag);
	
	// Add tag to screen
	if (manual){		
		// iterate all tags 
		var found=false;
		// if has tags
		if ( $('#tagsContainer').children().length >0 ){
			
			// iterate classes of tag
			$.each($('#tagsContainer').children(), function(childNumber, tag){
				
				var tagSecond;
				//iterate all classes of the tag
				$.each( tag.classList, function(classIndex, className){
					if (jQuery.isNumeric(className)) {
				    	console.log('there is class: '+ parseInt(className) );
				    	tagSecond = parseInt(className);
						// for break jquery foreach
						return false;
				    }
				});
				
				// check the second
				if ( sec < tagSecond){
					//finally add it before this child
					itemTag.insertBefore( $('#tagsContainer').children()[childNumber] );	//--Working
					found=true;
					return false;	//jquery $.each() break
				}			
				
			});
			if (!found){
				// if this this tag need to be at the end
				$('#tagsContainer').append(itemTag);
				//return false;	//jquery $.each() break
			}
		}else{
			// if there is no tags
			$('#tagsContainer').append(itemTag);
		}	
	}else{
		tagsContainer.append(itemTag);
	}
}
function init_tags_section(){
	$.each(videoJson.elements, function(sec, objects){	//2 times worst case
		if (objects.hasOwnProperty('tags') ){
			
			//iterate tags in same second
			$.each(objects.tags, function(i, tag){
				appendNewTagToTags(tag,sec, false);
			});
		}
	});
	
	// Create event when pressing 'enter' key on the keyboard while typing in the input, activate the search button
	$('#addTagText').keypress(function(event){
		if(event.keyCode == 13){
			$('#addTagButton').click();
		}
	});
}

function elementClick(atSec){
	$("#secondSlider").val(atSec);
	$("#secondSlider").trigger("input");
	$("#secondSlider").change();
}

function favoritesHandler(){
	$('#favoritesButtonSpan').toggleClass('favoritesButtonClicked');
	
	$.ajax({
		type : "Post",
		url : 'http://lecturus.herokuapp.com/auxiliary/addRemoveFavorites',
		data : {
			sessionId : videoJson.sessionId,
			userId : userEmail
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				// all is good.
			} else {
				// roll back
				$('#favoritesButtonSpan').toggleClass('favoritesButtonClicked');
				console.error('server status code are not 1');
			}
		},
		error : function(objRequest, errortype) {
			console.error(errortype);
			console.error("Can't do because: " + errortype);
		}
	});
}

function set_owner_profile(mail) {
	$.ajax({
		type : "Post",
		url : 'http://lecturus.herokuapp.com/users/getUser',
		data : {
			email : mail
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				var fullName;
				if (data.info.name != '') {
					fullName = data.info.name;
				}
				if (data.info.lastName != 'undefined') {
					if ( typeof fullName != 'undefined') {
						fullName += ' ' + data.info.lastName;
					}
				}
				$('#ownerFullName').text(fullName);
				$('#profileBody').attr('src', data.info.image);
			} else {
				console.error('server status code are not 1');
			}
		},
		error : function(objRequest, errortype) {
			console.error(errortype);
			console.error("Can't do because: " + errortype);
		}
	});
}

function doEverySecond() {
	console.log(video_timeCounter);
	$("#secondSlider").val(video_timeCounter);
	$('#currentTime').html(secondToTime(video_timeCounter));

	// Update input-range fill before and after thumb
	var val = (video_timeCounter - $("#secondSlider").attr('min')) / ($("#secondSlider").attr('max') - $("#secondSlider").attr('min'));
	changeProgressColor(val);

	if (fullMode) {
		// Check if there is photos with 'video_timeCounter' key
		if (videoJson.elements.hasOwnProperty( String(video_timeCounter))  ) {
			$.each(videoJson.elements[video_timeCounter], function(key, val) {
				switch (key) {
				case "photo":
					var photoUrl = videoJson.elements[video_timeCounter].photo.url;
					console.log('find in key: ' + key + ' ' + photoUrl);
					$('#viewPhotos').attr("src", photoUrl);
					// $('#lightboxLink').attr('href', photoUrl);
					break;
				}
			});
		}
		// Show tags
		checkIfThereAreTagsInSecond_AndShowThem(video_timeCounter);
	}
	//update slider
	video_timeCounter = parseInt($("#secondSlider").val()) + 1;
}
function checkIfThereAreTagsInSecond_AndShowThem(sec){
	// Check if there is tags with 'video_timeCounter' class
	if ( $('[data-sec='+sec+']').length >0){
		viewerTag(sec);
	}
}

function secondSlider_Handler() {
	console.log('secondSlider_Handler');
	$("#secondSlider").on("input", function() {
		/* callback at click-down and slidingb */ 
		// get value as sliding - use for show brief images & find witch sound to use
		console.log("input");
		//pause video
		mediaSound.pause();

		//show second to user
		secondSlider = this.value;
		video_timeCounter = secondSlider;
 
		$('#currentTime').html(secondToTime(secondSlider));
		console.log("seek to: " + secondSlider);
 
		// find witch sound contain the second user seek
		$.each(videoJson.audios, function(audioObj) {
			if ((secondSlider >= videoJson.audios[audioObj].startAt) && (secondSlider <= videoJson.audios[audioObj].startAt + videoJson.audios[audioObj].length)) {
				bestMatch = audioObj;
			}
		});
 
		//get the sound in this second-val + seconds_to_forward
		calcSeek = (secondSlider - videoJson.audios[bestMatch].startAt);
		soundContain = videoJson.audios[bestMatch].url;
		console.log("This second located in: " + soundContain);
		console.log("Need to seek from start: " + calcSeek);
	});

	$("#secondSlider").on("change", function() {
		/* callback at click-up */
		if (fullMode) {
			// find if in this second there is something to show
			findView();
		}
		
		// play the fuckaaaa sound (and run the interval)
		console.log("change");
		if (audio_timeCounter == bestMatch) {
			//seek in the same song
			//do the seek & auto play
			playAudio_withSeek(calcSeek, false);
		} else {
			//seek to another song
			//load & prepare sound
			//change source to: 'videoJson.audios[bestMatch].sound' and seek: 'calcSeek'
			//do the seek & auto play
			audio_timeCounter = bestMatch;
			playAudio_withSeek(calcSeek, true);
		}
	});
}

function playAudio_withSeek(timeToSeek, seekAnotherSong) {
	//debugger;
	if (seekAnotherSong) {
		//seek to another sound
		console.log("seek to other song");
		$("#audioSrc").attr("src", videoJson.audios[audio_timeCounter].url);
		seek = true;
		mediaSound.load();
	} else {
		//seek in the same sound
		console.log("seek in the song");
		mediaSound.play();
	}
	mediaSound.currentTime = timeToSeek;
}

/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/

// Media Player using HTML5's Media API
function initialiseMediaPlayer() {
	console.log("initialiseMediaPlayer");

	// Get handles to each of the buttons and required elements
	playPauseBtn = $('#play')[0];
	muteBtn = $('#mute')[0];

	// Hide the browser's default controls
	mediaSound.controls = false;

	mediaSound.oncanplaythrough = function() {
		console.log("oncanplaythrough");
		if (seek || currentEnd_loadSecond) {
			console.log("seek || currentEnd_loadSecond");
			seek = false;
			currentEnd_loadSecond = false;
			mediaSound.play();
		}
	};
	mediaSound.onplay = function() {
		console.log("my onPlay");
		if (videoJson.audios.length > 0) {
			if (fullMode) {
				showOp();
				if (!viewsWasIncrement) {
					incrementViewes();
				}
			}
			changeButtonType(playPauseBtn, 'pause');
			starttimeMS = new Date();

			if (resttimeMS < 1000 && resttimeMS > 0) {
				console.log("wait: " + resttimeMS);
				setTimeout(doEverySecond, resttimeMS);
			}
			if (interval == null) {
				interval = setInterval(doEverySecond, 1000);
			}
			console.log("#secondSlider val "+ $("#secondSlider").val() );
		} else {
			console.error('Error: there is no mp3 in the json-audios to play!');
			mediaSound.pause();
		}
	};
	mediaSound.onpause = function() {
		console.log("my onPause");
		if (fullMode)
			showOp();
		changeButtonType(playPauseBtn, 'play');
		clearInterval(interval);
		interval = null;
		endtimeMS = new Date();
		totaltimeMS = endtimeMS - starttimeMS;
		resttimeMS = 1000 - ((endtimeMS - starttimeMS) % 1000);
	};
	mediaSound.onended = function() {
		console.log("onended");
		audio_timeCounter++;

		// If thare are more videos
		if (audio_timeCounter < videoJson.audios.length) {

			// Change audio-source & Load func & Play func
			playAudio_withSeek(0, true);
			currentEnd_loadSecond = true;
		} else {
			// video has finished, 
			if (fullMode)
				showOp('end');
			//init param:
			clearInterval(interval);
			interval = null;
			audio_timeCounter = 0;
			video_timeCounter = 0;
			console.log("video finish");
		}
	};
	mediaSound.onerror = function(e) {
		console.log('Error loading: ' + e.target.src);
		alert("Error! Something went wrong");
		alert("Cannot play video because load failed.");
		//mediaSound.pause();
	};
	mediaSound.onvolumechange = function() {
		console.log("volumechange");
		// Update the button to be mute/unmute
		if (mediaSound.muted)
			changeButtonType(muteBtn, 'unmute');
		else
			changeButtonType(muteBtn, 'mute');
	};
}

// Use only in playmovie.html
// use to find views to show at input range current second
function findView() {
	console.log("findView");
	
	var sec = $("#secondSlider").val();
	
	// Show the last pic and the tags too if exist
	for ( i=sec; i>=0; i--) {
		
		//tags
		checkIfThereAreTagsInSecond_AndShowThem(i);
		
		//photos
		if (videoJson.elements.hasOwnProperty( String(i) )) {
			if (videoJson.elements[i].hasOwnProperty('photo') ) {
				$('#viewPhotos').attr("src", videoJson.elements[i].photo.url);
				break;
																				console.log("need to show image:" + $('#viewPhotos').attr("src"));
			}else if (i==0){
				$('#viewPhotos').attr("src", default_poster);
				removeViewerTag();
			}
		}
	}
}

function viewerTag(sec) {
	removeViewerTag();
	var tags = $('[data-sec='+sec+']').toggleClass('highlight');

	// Scroll div to the tag
	console.log('scroll to ---->'+sec);
	
	var toScroll;
	var currIndex = $('.'+sec).index();		//check if working good - if not - roll back to /////var currIndex = $('[name='+sec+']').index(); 
	if (currIndex>0){
		// Find first child before the current index
		// Desktop
		toScroll = $('#tagsContainer').children().eq(currIndex-1);

		// if (isMobile()){
			// // Mobile
			// // console.log('tag position: left: %d, top: %d',toScroll.position().left, toScroll.position().top);
			// toScroll = (currIndex-1)*$('#tagsContainer').children().eq(currIndex).height();
			// console.log('toScroll '+toScroll);
		// }
		
		$('#tagsContainer').scrollTo( toScroll );
	}else{
		//toScroll = $('[name='+sec+']');
		// scroll just 5px for the first
		$('#tagsContainer').scrollTo( 5 );
	}
}
function isMobile(){
	return true;
}
function removeViewerTag() {
	$('.highlight').toggleClass('highlight');
}

function togglePlayPause() {
	// If the mediaSound is currently paused or has ended
	if (mediaSound.paused || mediaSound.ended) {
		// Change the button to be a pause button
		changeButtonType(playPauseBtn, 'pause');
		// Play the media
		mediaSound.play();
	}
	// Otherwise it must currently be playing
	else {
		// Change the button to be a play button
		changeButtonType(playPauseBtn, 'play');
		// Pause the media
		mediaSound.pause();
	}
}

// Stop the current media from playing, and return it to the start position
function stopPlayer() {
	resetPlayer();
}

// Changes the volume on the media player
function changeVolume(direction) {
	if (direction === '+')
		mediaSound.volume += mediaSound.volume == 1 ? 0 : 0.1;
	else
		mediaSound.volume -= (mediaSound.volume == 0 ? 0 : 0.1);
	mediaSound.volume = parseFloat(mediaSound.volume).toFixed(1);
}

// Toggles the media player's mute and unmute status
function toggleMute() {
	if (mediaSound.muted) {
		// Change the cutton to be a mute button
		changeButtonType(muteBtn, 'mute');
		// Unmute the media player
		mediaSound.muted = false;
	} else {
		// Change the button to be an unmute button
		changeButtonType(muteBtn, 'unmute');
		// Mute the media player
		mediaSound.muted = true;
	}
}

// Replays the media currently loaded in the player
function replayMedia() {
	resetPlayer();
	mediaSound.play();
}

// Updates a button's title, innerHTML and CSS class to a certain value
function changeButtonType(btn, value) {
	switch(value) {
	case 'play':
		btn.children[0].className = 'glyphicon glyphicon-play';
		break;
	case 'pause':
		btn.children[0].className = 'glyphicon glyphicon-pause';
		break;
	case 'mute':
		btn.className = 'btn btn-sm glyphicon glyphicon-volume-off';
		break;
	case 'unmute':
		btn.className = 'btn btn-sm glyphicon glyphicon-volume-off btn-warning';
		break;
	}
	btn.title = value;
	btn.id = value;
}

// Loads a video item into the media player
function loadVideo() {
	for (var i = 0; i < arguments.length; i++) {
		var file = arguments[i].split('.');
		var ext = file[file.length - 1];
		// Check if this media can be played
		if (canPlayVideo(ext)) {
			// Reset the player, change the source file and load it
			resetPlayer();
			mediaSound.src = arguments[i];
			mediaSound.load();
			break;
		}
	}
}

// Checks if the browser can play this particular type of file or not
function canPlayVideo(ext) {
	var ableToPlay = mediaSound.canPlayType('video/' + ext);
	if (ableToPlay == '')
		return false;
	else
		return true;
}

// Resets the media player
function resetPlayer() {
	changeButtonType(playPauseBtn, 'play');
	clearInterval(interval);
	interval = null;
	
	
	// Reset the progress bar to 0
	$("#secondSlider").val(0);
	if (fullMode) {
		$('#viewPhotos').attr("src", default_poster);
		removeViewerTag();
		
		$('#currentTime').text('0:00');
	}
	//******* video has finished, init param:
	audio_timeCounter = 0;
	video_timeCounter = 0;
	console.log("video stoped");
	$("#audioSrc").attr("src", videoJson.audios[audio_timeCounter].url);
	
	//init params
	seek=false; 
	currentEnd_loadSecond=false;
	
	mediaSound.load();
	//**************************************
	
	changeProgressColor(0);

	// Move the media back to the start
	mediaSound.currentTime = 0;

	// Ensure that the play pause button is set as 'play'
	changeButtonType(playPauseBtn, 'play');
}

function changeProgressColor(val) {
	$("#secondSlider").css('background-image', '-webkit-gradient(linear, left top, right top, ' + 'color-stop(' + val + ', #27eee7), ' + 'color-stop(' + val + ', #666666)' + ')');
}

/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/
// Use only in playmovie.html
function showOp(t) {
	//debugger;
	//var tt = !!t;
	console.log('showOp audio_timeCounter='+audio_timeCounter)
	if (!!t && audio_timeCounter == videoJson.audios.length) {
		//append img child for 'view' section
	
		if (mediaSound.paused || mediaSound.ended) {
			// Pause button
			$('#viewOp').attr('src', 'includes/img/pauseCircle.png');
		}
		// Otherwise it must currently be playing
		else {
			// Play image
			$('#viewOp').attr('src', 'includes/img/playCircle.png');
		}
	
		// Change location
		$('#viewOp').removeAttr("style");
		$('#viewOp').css('position', 'absolute');
		var position = $('#viewContainer').position();
		var videoHeight = $('#viewContainer').height();
		var videoWidth = $('#viewContainer').width();
		var centerVideo = {
			fromTop : position.top + videoHeight / 2 - op.height / 2,
			fromLeft : position.left + videoWidth / 2 - op.width / 2
		};
		$('#viewOp').css('top', centerVideo.fromTop + 'px');
		$('#viewOp').css('left', centerVideo.fromLeft + 'px');
	
		// show play/pause pic for 400 millisecond
		var timesRun = 0;
		var interval = setInterval(function() {
			timesRun += 1;
			if (timesRun === 1) {
				clearInterval(interval);
				$('#viewOp').removeAttr('src');
				$('#viewOp').hide();
			}
			//do whatever here..
		}, 400);
	}
}

// Use only in playmovie.html
function show_edit_button_if_admin_or_participants() {
	if (isAllowToEdit(videoJson, userEmail)) {
		btn = $(
			"<button type='button' id='editButton' class='btn btn-xs btn-info ' title='click to edit this video' aria-label='' onclick='goto_editPage();'>"
			+"			<span class='glyphicon glyphicon-pencil leftIcon' aria-hidden='true'></span>edit"
			+"		</button>"
		);
			
		$('#viewOptions').append(btn);
		console.log('show button');
	}
}



// Use only in playmovie.html
function goto_editPage() {
	window.location.href = "editVideo.html?videoId=" + videoJson.sessionId;
}

// Use only in playmovie.html
function set_user_vote(tagElement, val) {
	//debugger;
	var elementId = tagElement.id;
	if (elementId == 'movieLikeButton' || elementId == 'movieUnlikeButton'){
		// Vote for movie
		movie_vote(val);
	}else{
		// Vote for tag
		tag_vote(tagElement, val);
	}
}

// Use only in playmovie.html
function movie_vote(val) {

	// Update Rating
	$.ajax({
		type : "Post",
		url : 'http://lecturus.herokuapp.com/session/updateSessionRating',
		data : {
			sessionId : videoJson.sessionId,
			email : userEmail,
			rating : val
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				//all is good.
				video_roll_back_current_vote();
				
				var rating = data.res;
				// Votes
				$('#movieUnlikeVal').text(rating.negative.value);
				$('#movieLikeVal').text(rating.positive.value);
				if (rating.negative.users) {
					//set green opacity
					$('#movieLikeButton').addClass('opacityButton');
					console.log('user has unlike this video');
			
				} else if (rating.positive.users) {
					//set red opacity
					$('#movieUnlikeButton').addClass('opacityButton');
					console.log('user has like this video');
				}
			} else {
				console.log('server status code are not 1, ' + data);
			}
		},
		error : function(objRequest, errortype) {
			console.log(errortype);
			console.log("Can't do because: " + errortype);
		}
	});
}
function video_roll_back_current_vote(){
	// Roll back new vote
	if ( $('#movieUnlikeButton').hasClass('opacityButton') ){
		$('#movieUnlikeButton').removeClass('opacityButton');
	}else if ( $('#movieLikeButton').hasClass('opacityButton') ){
		$('#movieLikeButton').removeClass('opacityButton');
	}
}
function tag_roll_back_current_vote(unlikeButton, likeButton){
	// Reset buttons state
	if ( unlikeButton.hasClass('opacityButton') ){
		unlikeButton.removeClass('opacityButton');
	}else if ( likeButton.hasClass('opacityButton') ){
		likeButton.removeClass('opacityButton');
	}
}

// Use only in playmovie.html
function tag_vote(tagElement, val) {
	var tag = tagElement.parentElement.parentElement.parentElement;
	var tagId = tag.id;
	var tagSecond;
	$.each( tag.classList, function(classIndex, className){
		if (jQuery.isNumeric(className)) {
	    	tagSecond = parseInt(className);
			// for break jquery foreach
			return false;
	    }
	});
	
	
	// Update Rating
	$.ajax({
		type : "Post",
		url : 'http://lecturus.herokuapp.com/tags/updateTagRating',
		data : {
			sessionId : videoJson.sessionId,
			time : tagSecond,
			tagId : tagId,
			userId : userEmail,
			vote : val
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				//all is good.
				debugger;
				var unlikeButton = $('#'+tagId+' section.voteContainerLine section.voteContainer button.unlikeButton');
				var likeButton = $('#'+tagId+' section.voteContainerLine section.voteContainer button.likeButton');
				
				tag_roll_back_current_vote(unlikeButton, likeButton);
				
				var unlikeButtonVal = $('#'+tagId+' section.voteContainerLine section.voteContainer button.unlikeButton span#tagUnlikeVal.unlikeVal');
				var likeButtonVal = $('#'+tagId+' section.voteContainerLine section.voteContainer button.likeButton span#tagLikeVal.likeVal');
				
				var rating = data.rating;
				// Votes
				unlikeButtonVal.text(rating.negative.value);
				likeButtonVal.text(rating.positive.value);
				
				if (rating.negative.users) {
					//set green opacity
					likeButton.addClass('opacityButton');
					console.log('user has unlike this video');
			
				} else if (rating.positive.users) {
					//set red opacity
					unlikeButton.addClass('opacityButton');
					console.log('user has like this video');
				}
			} else {
				console.log('server status code are not 1, ' + data);
			}
		},
		error : function(objRequest, errortype) {
			console.log(errortype);
			console.log("Can't do because: " + errortype);
		}
	});
}

// Use only in playmovie.html
function followToUser(tag) {
	// Update Rating
	$.ajax({
		type : "Post",
		url : 'http://lecturus.herokuapp.com/users/addRemoveFollow',
		data : {
			email : userEmail,
			userToFollow : videoJson.owner
		},
		dataType : 'json',
		success : function(data) {
			debugger;
			if (data.status == 1) {
				//all is good.
				var tagText = $('#followText').text().toLowerCase();
				if (tagText == 'follow') {
					$('#followText').text('Unfollow');
				} else {
					$('#followText').text('Follow');
				}
			} else {
				console.warn('error: cannot increment views. ' + data.desc);
			}
		},
		error : function(objRequest, errortype) {
			console.error(errortype);
			console.error("Can't do because: " + errortype);
		}
	});
}

// Use only in playmovie.html
function incrementViewes() {
	// Update Rating
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/session/updateViews',
		data : {
			sessionId : videoJson.sessionId,
			userId : userEmail
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				//all is good.
				viewsWasIncrement = true;
			} else {
				console.warn('error: cannot increment views. ' + data.desc);
			}
		},
		error : function(objRequest, errortype) {
			console.error(errortype);
			console.error("Can't do because: " + errortype);
		}
	});
}

function addTag(){
	var secondClicked = video_timeCounter;
	var tagText = $('#addTagText').val();
	$('#addTagText').val('');			//remove the text
	
	// Update Rating
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/tags/insertTag',
		data : {
			sessionId: videoJson.sessionId,
			userId : userEmail,
			tagText : tagText,
			time : secondClicked
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				var tag = data.tag;
				//all is good.
				// Add tag to the all tags in right place at video_timeCounter
				debugger;
				appendNewTagToTags(tag, secondClicked, true);
				
				// Add tag-icon at the top of the input range in right second
				addReminderInTopOfRange('tag', tag, tag.timestamp);
				
			} else {
				console.warn("error: Can't add tag because. " + data.desc);
			}
		},
		error : function(objRequest, errortype) {
			console.error("Can't add tag because: " + errortype);
			$('#addTagText').val(tagText);
		}
	});
}

// if user want to remove one of his tags
function removeTag(xIcon){
	tag = xIcon.parentElement;
	var tagId = tag.dataset.id;
	$('#'+tagId).css('opacity',0.5);
	var tagSecond;
	$.each( tag.classList, function(classIndex, className){
		if (jQuery.isNumeric(className)) {
	    	tagSecond = parseInt(className);
			// for break jquery foreach
			return false;
	    }
	});
	// Update Rating
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/tags/deleteTag',
		data : {
			sessionId: videoJson.sessionId,
			userId : userEmail,
			tagId : tagId,
			time : tagSecond
		},
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				//all is good.
				// remove tag from all tags, and from reminders at top of range
				$('[data-id='+tagId+']').remove();
			} else {
				console.warn("error: Can't add tag because. " + data.desc);
			}
		},
		error : function(objRequest, errortype) {
			console.error("Can't add tag because: " + errortype);
			$('#addTagText').val(tagText);
		}
	});
}
