//###############################  Shared variables ###############################
var googleCallbackExecuted = false;
var userEmail;

//An array that holds the paths to various degree thumbnails images
var thumbnails = new Array();
thumbnails['10'] = {small: "includes/img/thumbnails/anglitSmall.png", medium: "includes/img/thumbnails/anglitMedium.png", large: "includes/img/thumbnails/anglitLarge.png", xl: "includes/img/thumbnails/anglitXL.png", css:{color: "#ff0913"}}; // Anglit
thumbnails['30'] = {small: "includes/img/thumbnails/handasaSmall.png", medium: "includes/img/thumbnails/handasaMedium.png", large: "includes/img/thumbnails/handasaLarge.png", xl: "includes/img/thumbnails/handasaXL.png", css:{color: "#27cb80"}}; //Handasa
thumbnails['31'] = {small: "includes/img/thumbnails/tvnSmall.png", medium: "includes/img/thumbnails/tvnMedium.png", large: "includes/img/thumbnails/tvnLarge.png", xl: "includes/img/thumbnails/tvnXL.png", css:{color: "#0183d6"}}; //Taasiya Ve Nihul
thumbnails['33'] = {small: "includes/img/thumbnails/himitSmall.png", medium: "includes/img/thumbnails/himitMedium.png", large: "includes/img/thumbnails/himitLarge.png", xl: "includes/img/thumbnails/himitXL.png", css:{color: "#df8c2e"}}; //Handasa himit
thumbnails['34'] = {small: "includes/img/thumbnails/plastikaSmall.png", medium: "includes/img/thumbnails/plastikaMedium.png", large: "includes/img/thumbnails/plastikaLarge.png", xl: "includes/img/thumbnails/plastikaXL.png", css:{color: "#bd30ff"}}; //Handasat Plastika
thumbnails['35'] = {small: "includes/img/thumbnails/tohnaSmall.png", medium: "includes/img/thumbnails/tohnaMedium.png", large: "includes/img/thumbnails/tohnaLarge.png", xl: "includes/img/thumbnails/tohnaXL.png", css:{color: "#00aed7"}}; //Handasat Tohna
thumbnails['36'] = {small: "includes/img/thumbnails/electronicaSmall.png", medium: "includes/img/thumbnails/electronicaMedium.png", large: "includes/img/thumbnails/electronicaLarge.png", xl: "includes/img/thumbnails/electronicaXL.png", css:{color: "#ff0913"}}; //Handasat Electronica
thumbnails['40'] = {small: "includes/img/thumbnails/tarbutSmall.png", medium: "includes/img/thumbnails/tarbutMedium.png", large: "includes/img/thumbnails/tarbutLarge.png", xl: "includes/img/thumbnails/tarbutXL.png", css:{color: "#3d87e3"}}; //Tarbut
thumbnails['41'] = {small: "includes/img/thumbnails/ofnaSmall.png", medium: "includes/img/thumbnails/ofnaMedium.png", large: "includes/img/thumbnails/ofnaLarge.png", xl: "includes/img/thumbnails/ofnaXL.png", css:{color: "#ff24b9"}}; //Ofna
thumbnails['42'] = {small: "includes/img/thumbnails/textilSmall.png", medium: "includes/img/thumbnails/textilMedium.png", large: "includes/img/thumbnails/textilLarge.png", xl: "includes/img/thumbnails/textilXL.png", css:{color: "#8bd0ca"}}; //Textil
thumbnails['43'] = {small: "includes/img/thumbnails/tahshitimSmall.png", medium: "includes/img/thumbnails/tahshitimMedium.png", large: "includes/img/thumbnails/tahshitimLarge.png", xl: "includes/img/thumbnails/tahshitimXL.png", css:{color: "#bdb834"}}; //Tahshitim
thumbnails['44'] = {small: "includes/img/thumbnails/hazutitSmall.png", medium: "includes/img/thumbnails/hazutitMedium.png", large: "includes/img/thumbnails/hazutitLarge.png", xl: "includes/img/thumbnails/hazutitXL.png", css:{color: "#fc5a45"}}; //hazutit
thumbnails['45'] = {small: "includes/img/thumbnails/taasiyatiSmall.png", medium: "includes/img/thumbnails/taasiyatiMedium.png", large: "includes/img/thumbnails/taasiyatiLarge.png", xl: "includes/img/thumbnails/taasiyatiXL.png", css:{color: "#c49da0"}}; //Itzuv Taasiyati
thumbnails['46'] = {small: "includes/img/thumbnails/mivneSmall.png", medium: "includes/img/thumbnails/mivneMedium.png", large: "includes/img/thumbnails/mivneLarge.png", xl: "includes/img/thumbnails/mivneXL.png", css:{color: "#ffaf30"}}; //Mivne Ve Sviva
thumbnails['47'] = {small: "includes/img/thumbnails/omanutSmall.png", medium: "includes/img/thumbnails/omanutMedium.png", large: "includes/img/thumbnails/omanutLarge.png", xl: "includes/img/thumbnails/omanutXL.png", css:{color: "#f54174"}}; //Omanut
thumbnails['49'] = {small: "includes/img/thumbnails/itzuvSecondSmall.png", medium: "includes/img/thumbnails/itzuvSecondMedium.png", large: "includes/img/thumbnails/itzuvSecondLarge.png", xl: "includes/img/thumbnails/itzuvSecondXL.png", css:{color: "#8d8d9d"}}; //second degree Itzuv

//###############################  Shared functions ###############################

/*****************************************************************************
 * Google sign-in callback - runs every time a connection STATE is changed   *
 * NOTE: Changes more then once per connection                               *
 ****************************************************************************/
function signinCallback(authResult) {
	//If the user cleared its cache and lost the organization value, or tried to 
	//enter the inner site pages without performing a proper login, redirect him to login page.(index.html)
		if(!window.localStorage.getItem("userOrg")){
			window.location.href = "index.html";
		}
		
	//Since this callback runs every time a connection status is changed, we want to make sure it runs only ONCE
	if (authResult['status']['signed_in'] && !googleCallbackExecuted) {
		googleCallbackExecuted = true;
		gapi.client.load('plus', 'v1', function() {
			var request = gapi.client.plus.people.get({
				'userId' : 'me'
			});
			
			request.execute(function(resp) {
				userEmail = resp.emails[0].value;
				googleResp = resp; //used in playmovie.js when user adds a tag
				initPage();
				$('#topNavProfilePic').css('background-image', "url(" + resp.image.url + ")");
				$("#userName").text(resp.displayName);
				
				//Sign out button click listener.
				$("#signOut").click(function() {
					console.log("signOut is clicked");
					gapi.auth.signOut();
				});
			});
		});
	} else if (!authResult['status']['signed_in']) {
		console.log('PROBLEM SIGNING IN: Sign-in state: ' + authResult['error']);
		window.location.href = "index.html";
	}
}

/**************************************************************************
 * This function creates a string that represents the top navigation bar  *
 * and pushes it to the DOM                                               *
 *************************************************************************/
function injectTopNav() {
 	var navHTML = 	  "	<div class='container-fluid'>"
			     	+ "		<div class='navbar-header'>"
			       	+ "			<button id='toggleMenu' type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar-collapse'>"
			     	+ "   			<span class='icon-bar'></span>"
			        + "  	 		<span class='icon-bar'></span>"
			        + "  	 		<span class='icon-bar'></span>"
			     	+ "  		</button>"
			     	+ "  		<a class='navbar-brand' href='home.html'>"
			     	+ "   			<img alt='Lecturus' src='includes/img/topNav_lecturusLogo.jpg'>"
			     	+ "  		</a>"
			    	+ " </div>"
			    	+ " <div class='collapse navbar-collapse' id='navbar-collapse'>"
			    	+ "   	<ul id='navPagesLinks' class='nav navbar-nav'>"
			     	+ "    		<li><a href='favorites.html' id='myFavoritesPage'>My Favorite Lectures</a></li>"
				 	+ "	 		<li><a href='history.html' id='myHistoryPage'>My Watch History </a></li>"
				 	+ "	  		<li><a href='myRecordings.html' id='myRecordingsPage'>My Recordings </a></li>"
			      	+ " 	</ul>"
			        + " 	<div id='searchHolder' class='navbar-form navbar-left'>"
			        + "  		<input id='searchQuery' type='text' class='form-control' placeholder='&#x1f50d; Search'>"
			       	+ "  	</div>"
			     	+ " 	<ul class='nav navbar-nav navbar-right'>"
			        + " 		<li id='navDropDown' class='dropdown'>"
			        + " 			<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>"
			        + "  				<section id='topNavProfilePic' class='profilePic'></section><span id='userName'></span> <img src='includes/img/topNav_dropdown.jpg'></a>"
			     	+ " 	<ul id='toggledMenuTopNav' class='dropdown-menu' role='menu'>"
			      	+ "  		<li><a href='#' id='profileBtn'>Profile</a></li>"
			        + " 		<li id='signOut'><a href='#'>Sign out</a></li>"
			        + " 	</ul>"
			    	+ " </div><!-- /.navbar-collapse -->"
			  		+ " </div><!-- /.container-fluid -->";
					  		
	$(document).ready(function() {
		$("#topNav").append(navHTML);
		
		$("#profileBtn").click(function(){
			window.location.href = "profile.html?user=" + userEmail;
		});
		
		//When pressing 'enter' key on the keyboard while typing in the input, activate the search button
		$('#searchQuery').keypress(function(event) {
			if (event.keyCode == 13) {
				console.log(window.location, $("#searchQuery").val());
				window.location.href = "search.html?searchQuery=" + $("#searchQuery").val();
			}
		});
	});
}

/**********************************************************
 * This function converts seconds to time string format.  *
 * Returns a string in format HH:MM:SS                    *
 *********************************************************/ 
function secondToTime(second) {
	var sec_num = parseInt(second, 10);

	var hours = Math.floor(sec_num / 3600);
	var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	var seconds = sec_num - (hours * 3600) - (minutes * 60);

	if (hours < 10) {
		hours = "0" + hours;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	var time = hours + ':' + minutes + ':' + seconds;

	// remove zone priffix
	for ( i = 0; i < 4; i++) {
		if (time[0] == '0' || time[0] == ':') {
			time = time.substring(1, time.length);
		}
	}
	return time;
}


/**********************************************************************
 * This function looks for a query parameters and returns its value   *
 *********************************************************************/
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	    results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



/***********************************************
 * This function converts a timestamp to a date*
 **********************************************/
function dataFromTimestamp(timestamp) {
	var d = new Date(timestamp);

	// Time
	var h = addZero(d.getHours());
	//hours
	var m = addZero(d.getMinutes());
	//minutes
	var s = addZero(d.getSeconds());
	//seconds

	// Date
	var da = d.getDate();
	//day
	var mon = d.getMonth() + 1;
	//month
	var yr = d.getFullYear();
	//year
	var dw = d.getDay();
	//day in week

	// Readable feilds
	months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
	var monName = months[d.getMonth()];
	//month Name
	var time = h + ":" + m + ":" + s;
	//full time show
	var thisDay = da + "/" + mon + "/" + yr;
	//full date show

	var dateTime = {
		seconds : s,
		minutes : m,
		hours : h,
		dayInMonth : da,
		month : mon,
		year : yr,
		dayInTheWeek : dw,
		monthName : monName,
		fullTime : time,
		fullDate : thisDay
	};
	return dateTime;

	function addZero(i) {
		if (i < 10) {
			i = "0" + i;
		}
		return i;
	}

}

/********************************************************************************
 * This function scroll a div some pixels or scroll it to a inner specific div  *
 *******************************************************************************/ 
$.fn.scrollTo = function(target, options, callback) {
	if ( typeof options == 'function' && arguments.length == 2) {
		callback = options;
		options = target;
	}
	var settings = $.extend({
		scrollTarget : target,
		offsetTop : 50,
		duration : 500,
		easing : 'swing'
	}, options);
	return this.each(function() {
		var scrollPane = $(this);
		var scrollTarget = ( typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
		var scrollY = ( typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
		scrollPane.animate({
			scrollTop : scrollY
		}, parseInt(settings.duration), settings.easing, function() {
			if ( typeof callback == 'function') {
				callback.call(this);
			}
		});
	});
};


function isAllowToEdit(getSessionByIdResponse, userEmail) {
	var hasPrivilage = false;
	if (userEmail == getSessionByIdResponse.owner) {
		// is admin
		hasPrivilage = true;

		//remove follow button - for dont make following to myself
		$('#followButton').remove();
	} else {
		$.each(getSessionByIdResponse.participants, function(i, participantsEmail) {
			if (userEmail == participantsEmail) {
				// is one of participants
				hasPrivilage = true;
			}
		});
	}
	return hasPrivilage;
}