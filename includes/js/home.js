var userEmail,
    getCourseVideosJSON,
    appendString = "",
    loopingKey,
    degreesAndCoursesJson,
    edtKey,
    ectKey,
    numOfTagsToPresent = 12,
    startFrom = -numOfTagsToPresent,
    stopAt = -1,
    canRemoveDegreeTag = true,
    canRemoveCourseTag = false,
    userOrg,
    googleCounter = 0,
    from = 0,
    to = 4,
    getMoreLectures = false,
    jsonRef,
    currentChosenDegreeTag;

// Configuring infinite scroll. When a desired scroll height is reached, call a function to get more videos.
$(window).scroll(function() {
	if (($(window).scrollTop() + $(window).height() > $(document).height() - 100) && getMoreLectures) {
		$("#loadingMoreMessage").show();
		loadMore();
	}
});


/* *********************************************************
 *                    Main function                 	   *
 * *********************************************************/
function initPage() {
	getMoreLectures = true;
	$(document).ready(function() {
		//initialize top nav in the html file
		injectTopNav();

		//Get user organization from local storage
		userOrg = window.localStorage.getItem("userOrg");

		//load the degrees and courses json file
		$.ajax({
			type : "POST",
			url : 'http://lecturus.herokuapp.com/auxiliary/getCoursesByOrg/',
			data : {
				"org" : userOrg
			},
			success : function(data) {
				degreesAndCoursesJson = data;
				if (degreesAndCoursesJson.status == 1) {
					populateDegreeTagsToExplore();

				}

				//click event for right arrow in explore
				$("#arrowRight").on("click", function() {
					if ($(this).hasClass('arrowEnabled')) {
						//empty explore tags holder to populate it with courses tags
						//run callback AFTER all elements are fade out to ensure the
						//code runs only one time and not once for each element
						$(".ect").fadeOut(400).last().queue(function() {
							$("#tagsHolder").empty();
							//load next pack of tags into explore
							showNextPackOfCourses();
						});

					}
				});

				//click event for left arrow in explore
				$("#arrowLeft").on("click", function() {
					if ($(this).hasClass('arrowEnabled')) {
						//empty explore tags holder to populate it with courses tags
						//run callback AFTER all elements are fade out to ensure the
						//code runs only one time and not once for each element
						$(".ect").fadeOut(400).last().queue(function() {
							$("#tagsHolder").empty();
							//load next pack of tags into explore
							showPreviousPackOfCourse();
						});

					}
				});

			},
			error : function(objRequest, errortype) {
				console.log("Cannot get video JSON file");
			}
		});
		//Show main videos in video wrapper
		showPopularAndFollowingVideos();

		//Check to see if the window is top if not then display button
		$(window).scroll(function() {
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});

		//Click event to scroll to top
		$('.scrollToTop').click(function() {
			$('html, body').animate({
				scrollTop : 0
			}, 800);
			return false;
		});

	});

}

/* ****************************************************************
 * this function populates explore tags holder with Degree tags   *
 * ****************************************************************
 */
function populateDegreeTagsToExplore() {
	$.each(degreesAndCoursesJson.degrees, function(key, val) {
		//edt = explore degree tag
		$("#tagsHolder").append($("<a></a>").attr({
			"class" : "tag edt",
			"id" : val.id +"edt" + key
		}).css({ 
			//setting dynamic css configurations
			"border" : "2px solid " + thumbnails[val.id].css.color,
			"color" : thumbnails[val.id].css.color,
			"background" : "#000",
			"cursor" : "pointer"
		}).html(val.name).hover(function(){
			$(this).css({
				//Mouse in
				"background" : $(this).css("color"),
				"color" : "#000"
			});
		}, function(){
			//Mouse out
			$(this).css({
				"color" : thumbnails[val.id].css.color,
				"background" : "#000"
				
			});
		}));
		
		
	
		//attaching click event to tags in explore section
		$(".edt").on("click", function() {
			canRemoveDegreeTag = true;
			
			//show the arrows
			$("#browseRight , #browseLeft").css("visibility", "visible");
			//save degree key of clicked element - split its id
			edtKey = $(this).attr('id').split('edt')[1];

			//Push the degree name chosen instead the 'Explore' title
			currentChosenDegreeTag = "<h2 id='chosenDegreeTag' class='chosenTag'>" + $(this).text() + "</h2><h2 class='plusSign'>&nbsp;+&nbsp;</h2>";
			$("#selectionStringHolder").empty().append(currentChosenDegreeTag);

			/*
			 * Empty explore tags holder to populate it with courses tags
			 * run callback AFTER all elements are fade out to ensure the
			 * code runs only one time and not once for each element
			 */
			$(".edt").fadeOut(400).last().queue(function() {
				//Populate Videos Wrapper
				populateExploreWrapper(degreesAndCoursesJson.degrees[edtKey].id);

				//setting view
				$("#exploreVideosWrapper").show();
				$("#popularVideosWrapper, #followingVideosWrapper").hide();

				//clear tags holder
				$("#tagsHolder").empty();
				//populate with courses that belong to clicked degree
				showFirstPackOfCourses(edtKey);
			});

			//click event listener for a chosen degree tag in the explore title
			selectedDegreeTagClickListener();

		});
	});
}

//
/* ******************************************************************************
 * This function appends the first set of courses to the 'explore tags holder   *
 * ******************************************************************************
 */
function showFirstPackOfCourses(edtKey) {
	//update parameters
	startFrom = stopAt + 1;
	stopAt += numOfTagsToPresent;

	//Case 1: There are MORE courses to present then the maximum required for the first pack
	if (degreesAndCoursesJson.degrees[edtKey].courses[stopAt] != undefined) {
		for (var i = startFrom; i <= stopAt; i++) {
		//ect = explore course tag
			$("#tagsHolder").append($("<a></a>").attr({
				"class" : "tag ect",
				"id" : "ect" + degreesAndCoursesJson.degrees[edtKey].courses[i].id
			}).css({ 
				//setting dynamic css configurations
				"border" : "2px solid " + thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"background" : "#000",
				"cursor" : "pointer"
			}).html(degreesAndCoursesJson.degrees[edtKey].courses[i].name).hover(function(){
				$(this).css({
					//Mouse in
					"background" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"color" : "#000"
				});
			}, function(){
				//Mouse out
				$(this).css({
					"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"background" : "#000"
					
				});
			}));
			
		}

		if (degreesAndCoursesJson.degrees[edtKey].courses[stopAt + 1] != undefined) {
			//remove old color style and add new class with new color style
			$("#arrowRight").removeClass('arrowDisabled').addClass('arrowEnabled');
		}

	}

	//Case 2: There are LESS courses to present then the maximum required for the first pack
	if (degreesAndCoursesJson.degrees[edtKey].courses[stopAt] <= degreesAndCoursesJson.degrees[edtKey].courses.length) {
		$.each(degreesAndCoursesJson.degrees[edtKey].courses, function(key, val) {
			var tmp = "<a href='#' class='tag ect' id='ect" + key + "'>" + val.name + "</a>";
			//ect = explore degree tag
			$("#tagsHolder").append(tmp);
		});
	}

	//click event for explore course tags
	exploreCourseTagsEvents();

}

/* ******************************************************************************
 * This function appends the next set of courses to the 'explore tags holder    *
 * ******************************************************************************
 */
function showNextPackOfCourses() {
	//update parameters
	startFrom = stopAt + 1;
	stopAt += numOfTagsToPresent;

	//Get next pack
	if (degreesAndCoursesJson.degrees[edtKey].courses[stopAt] != undefined) {
		for (var i = startFrom; i <= stopAt; i++) {
			//ect = explore course tag
			$("#tagsHolder").append($("<a></a>").attr({
				"class" : "tag ect",
				"id" : "ect" + degreesAndCoursesJson.degrees[edtKey].courses[i].id
			}).css({ 
				//setting dynamic css configurations
				"border" : "2px solid " + thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"background" : "#000",
				"cursor" : "pointer"
			}).html(degreesAndCoursesJson.degrees[edtKey].courses[i].name).hover(function(){
				$(this).css({
					//Mouse in
					"background" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"color" : "#000"
				});
			}, function(){
				//Mouse out
				$(this).css({
					"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"background" : "#000"
					
				});
			}));
		}

		if (degreesAndCoursesJson.degrees[edtKey].courses[stopAt + 1] == undefined) {
			//change color of the arrow to a disabled arrow colors scheme
			$("#arrowRight").removeClass('arrowEnabled').addClass('arrowDisabled');
		}

		//If we reached the end then dont allow clicking the right arrow
		if ((degreesAndCoursesJson.degrees[edtKey].courses.length) % startFrom == 0) {
			//change color of the arrow to a disabled arrow colors scheme
			$("#arrowRight").removeClass('arrowEnabled').addClass('arrowDisabled');
		}
	}

	// last chunk that is smaller then the gap between startFrom & stopAt
	else if ((degreesAndCoursesJson.degrees[edtKey].courses.length) % startFrom > 0) {

		//populate tags holder with 'left overs' tags
		for (var i = startFrom; i < (startFrom + (degreesAndCoursesJson.degrees[edtKey].courses.length) % startFrom); i++) {
			//ect = explore course tag
			$("#tagsHolder").append($("<a></a>").attr({
				"class" : "tag ect",
				"id" : "ect" + degreesAndCoursesJson.degrees[edtKey].courses[i].id
			}).css({ 
				//setting dynamic css configurations
				"border" : "2px solid " + thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"background" : "#000",
				"cursor" : "pointer"
			}).html(degreesAndCoursesJson.degrees[edtKey].courses[i].name).hover(function(){
				$(this).css({
					//Mouse in
					"background" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"color" : "#000"
				});
			}, function(){
				//Mouse out
				$(this).css({
					"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"background" : "#000"
					
				});
			}));
		}

		//change color of the arrow to a disabled arrow colors scheme
		$("#arrowRight").removeClass('arrowEnabled').addClass('arrowDisabled');
	}

	//remove old color style and add new class with new color style
	$("#arrowLeft").removeClass('arrowDisabled').addClass('arrowEnabled');

	//click event for explore course tags
	exploreCourseTagsEvents();
}



/* *********************************************************************************
 * This function appends the previous set of courses to the 'explore tags holder   *
 * *********************************************************************************
 */
function showPreviousPackOfCourse() {
	if (degreesAndCoursesJson.degrees[edtKey].courses[startFrom - numOfTagsToPresent - 1] == undefined) {
		//change color of the arrow to a disabled arrow colors scheme
		$("#arrowLeft").removeClass('arrowEnabled').addClass('arrowDisabled');
	}

	if (degreesAndCoursesJson.degrees[edtKey].courses[stopAt + 1] == undefined) {
		//remove old color style and add new class with new color style
		$("#arrowRight").removeClass('arrowDisabled').addClass('arrowEnabled');

	}

	//update parameters
	stopAt = startFrom - 1;
	startFrom -= numOfTagsToPresent;

	//populate tags holder
	for (var i = startFrom; i <= stopAt; i++) {
		//ect = explore course tag
			$("#tagsHolder").append($("<a></a>").attr({
				"class" : "tag ect",
				"id" : "ect" + degreesAndCoursesJson.degrees[edtKey].courses[i].id
			}).css({ 
				//setting dynamic css configurations
				"border" : "2px solid " + thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
				"background" : "#000",
				"cursor" : "pointer"
			}).html(degreesAndCoursesJson.degrees[edtKey].courses[i].name).hover(function(){
				$(this).css({
					//Mouse in
					"background" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"color" : "#000"
				});
			}, function(){
				//Mouse out
				$(this).css({
					"color" : thumbnails[degreesAndCoursesJson.degrees[edtKey].id].css.color,
					"background" : "#000"
					
				});
			}));
	}

	//click event for explore course tags
	exploreCourseTagsEvents();
}



/* *************************************************************************
 * This function attaches events to course tags in explore tags holder	   *
 * *************************************************************************
 */
function exploreCourseTagsEvents() {
	//remove any previous listeners
	//$(".ect").unbind();

	//click event for explore course tags
	$(".ect").on("click", function() {
		//unbind click event from the chosen degree tag
		$("#chosenDegreeTag").unbind();

		
		//save degree key of clicked element - split its id
		ectKey = $(this).attr('id').split('ect')[1];

		canRemoveCourseTag = true;
		canRemoveDegreeTag = false;

		//Append selected course tag text to the explore title
		$("#selectionStringHolder").html(currentChosenDegreeTag + " " + "<h2 id='chosenCourseTag' class='chosenTag'>" +  $(this).text() + "</h2>");
		
		//re-configure css style
		$("#chosenDegreeTag").removeClass('chosenTag').addClass('chosenTagDisabledClick');
		
		//Populate Videos Wrapper
		populateExploreWrapper(degreesAndCoursesJson.degrees[edtKey].id, ectKey);

		//remove any previous listeners
		$("#chosenCourseTag").off();
		//click event handler for selected course tag in explore title
		$("#chosenCourseTag").on("click", function() {
			//Populate Videos Wrapper
			populateExploreWrapper(degreesAndCoursesJson.degrees[edtKey].id);
			//remove chosen course tag from the explore title
			$(this).remove();

			//Allow removing a degreeTag from the explore title
			canRemoveCourseTag = false;
			canRemoveDegreeTag = true;

			//re-configure css style
			$("#chosenDegreeTag").removeClass('chosenTagDisabledClick').addClass('chosenTag');
			
			//Add chosen degree tag a click listener because it is off
			selectedDegreeTagClickListener();
			
			//show the hidden (and already populated) explore
			$("#exploreBody").show();

		});

	});
}



/* ********************************************************************************
 * This function recieves a degree number OR a degree number + course number	  *
 * Then it retrieves the relative videos and appends it to the DOM                *
 * ********************************************************************************
 */
function populateExploreWrapper(degreeNum, courseNum) {
	//Show loading spinner
	$(".loading").show();

	var query = "?email=" + userEmail + "&degree=" + degreeNum + "&course=" + courseNum || 0;
	//first clear Video Wrapper from any videos and then append new videos
	$("#exploreVideosWrapper").empty();

	//get videos
	$.ajax({
		type : "GET",
		url : 'http://lecturus.herokuapp.com/auxiliary/getSessionsByCourse' + query,
		dataType : 'json',
		success : function(data) {
			if (data.status == 1) {
				getCourseVideosJSON = data;
				jsonRef = data;
				createTemplateAndAppendVideos(data.res, "#exploreVideosWrapper");

				//Hide loading spinner
				$(".loading").hide();
			}
		},
		error : function(objRequest, errortype) {
			console.log("Cannot get video Json");
		}
	});
}

/***********************************************************
 * This function recieves an ARRAY of videos and creates a *
 * HTML template whic is finally being append to the DOM   *
 ************************************************************
 */
function createTemplateAndAppendVideos(arrayOfVideos, appendTo) {

	appendString = "";
	var counter = -1;
	//first clear Video Wrapper from any videos and then append new videos
	$(appendTo).empty();

	if (getCourseVideosJSON) {
		//show the user the number of results
		appendString += "<section  id='searchResults'><span>  " + getCourseVideosJSON.count + " lectures match your search</span></section>";
		getCourseVideosJSON = null;
	}
	$.each(arrayOfVideos, function(key, val) {
		counter++;
		//Start new row of videos
		if ((counter % 4) == 0) {

			appendString += "<section class='row'> " + " <section class='col-md-3'>" + "<section class='singleVideoWrapper'>" + "<section class='videoImage'>" + "<a href='playmovie.html?videoId="
			 + val.sessionId + "'><img src='" + thumbnails[val.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";

	
			appendString += "<section class='videoTitle'><a href='playmovie.html?videoId=" + val.sessionId + "'>" + val.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val.degree + " , " + val.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val.lecturer + "</section></section>" 
			+ "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val.views + " </section>";
					//populate participants section
			//Add current video owner profile picture as a prticipant
			appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val.owner].image + "' title='" + jsonRef.users[val.owner].name + " " + jsonRef.users[val.owner].lastName + "' /></a>";

			//Add other particimapnts profile pictures
			$.each(val.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
			});
			appendString += "</section>"+ "	  </section>" + " </section>";

		}
		//otherwise append to the existong row of videos
		else {
			appendString += " <section class='col-md-3'>" + "	<section class='singleVideoWrapper'>" + " <section class='videoImage'>" + "<a href='playmovie.html?videoId=" + val.sessionId + "'><img src='" + thumbnails[val.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";
			appendString += "<section class='videoTitle'><a href='playmovie.html?videoId=" + val.sessionId + "'>" + val.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val.degree + " , " + val.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val.lecturer + "</section></section>" + "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val.views + "</section>"; 
			//populate participants section
			//Add current video owner profile picture as a prticipant
			appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val.owner].image + "' title='" + jsonRef.users[val.owner].name + " " + jsonRef.users[val.owner].lastName + "' /></a>";

			//Add other particimapnts profile pictures
			$.each(val.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
			});
			appendString += "</section>"+ "	  </section>" + " </section>";

			//if the next item opens a new row, Close current  row section
			if ((counter + 1) % 4 == 0) {
				appendString += "</section>";
			}

		}
	});

	//Close last row at the end
	if ((counter + 1) % 4 != 0) {
		appendString += "</section>";

	}

	//finally, append  everything
	$(appendTo).append(appendString);

}

/***********************************************************
 * --------------FOLLOWING USERS VIDEOS TEMPLATE-----------*
 * This function recieves an ARRAY of videos and creates a *
 * HTML template whic is finally being append to the DOM   *
 ***********************************************************
 */
function createTemplateAndAppendFollowingVideos(arrayOfVideos, appendTo) {
	appendString = "";
	var counter = -1;
	//first clear Video Wrapper from any videos and then append new videos

	$.each(arrayOfVideos, function(key, val) {
		//Followed user name and profile picture
		appendString += "<section class='fwPicAndName'><a href='profile.html?user=" + jsonRef.users[key].email + "'><img class='profilePic' 	src='" + jsonRef.users[key].image + "'><span> " + jsonRef.users[key].name + " " + jsonRef.users[key].lastName + "</span></a></section>";

		$.each(val, function(key3, val3) {
			counter++;
			//Start new row of videos
			if ((counter % 4) == 0) {
				appendString += "<section class='row'> " + " <section class='col-md-3'>" + "	<section class='singleVideoWrapper'>" + "		 <section class='videoImage'>" + "<a href='playmovie.html?videoId=" + val3.sessionId + "'><img src='" + thumbnails[val3.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val3.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";
				
				appendString +=  "		<section class='videoTitle'><a href='playmovie.html?videoId=" + val3.sessionId + "'>" + val3.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val3.degree + " , " + val3.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val3.lecturer + "</section></section>" + "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val3.views + " </section>";
				//populate participants section
				//Add current video owner profile picture as a prticipant
				appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val3.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val3.owner].image + "' title='" + jsonRef.users[val3.owner].name + " " + jsonRef.users[val3.owner].lastName + "' /></a>";

				//Add other particimapnts profile pictures
				$.each(val3.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
				});
				appendString += "</section>"  + "	  </section>" + " </section>";

			}
			//otherwise append to the existong row of videos
			else {
				appendString += " <section class='col-md-3'>" + "	<section class='singleVideoWrapper'>" + "		 <section class='videoImage'>" + "<a href='playmovie.html?videoId=" 
				+ val3.sessionId + "'><img src='" + thumbnails[val3.degreeId].medium + "'></a></section><section class='videotimeHolder'>" + secondToTime(val3.totalSecondLength) + "  </section>" + "	<section class='contentHolder'>";

				appendString += "		<section class='videoTitle'><a href='playmovie.html?videoId=" + val3.sessionId + "'>" + val3.title + "</a></section>" + "		<section class='videoDetails'><span class='smallAndBold'>Tags:</span>" + val3.degree + " , " + val3.course + "</section>" + "		<section class='videoLecturer'><span class='smallAndBold'>Lecturer:</span> " + val3.lecturer + "</section></section>" + "		<section class='videoViews'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>" + val3.views + "</section>";
								//populate participants section
				//Add current video owner profile picture as a prticipant
				appendString += "<section class='videoParticipants'><a href='profile.html?user=" + jsonRef.users[val3.owner].email + "'><img class='profilePic ' src='" + jsonRef.users[val3.owner].image + "' title='" + jsonRef.users[val3.owner].name + " " + jsonRef.users[val3.owner].lastName + "' /></a>";

				//Add other particimapnts profile pictures
				$.each(val3.participants, function(key2, val2) {
				appendString += "<a href='profile.html?user=" + jsonRef.users[val2].email + "'><img class='profilePic ' src='" + jsonRef.users[val2].image + "' title='" + jsonRef.users[val2].name + " " + jsonRef.users[val2].lastName + "' /></a>";
				});
				appendString += "</section>"+ "	  </section>" + " </section>";

				//if the next item opens a new row, Close current  row section
				if ((counter + 1) % 4 == 0) {
					appendString += "</section>";
				}
			}
		});

		//Close last row at the end
		if ((counter + 1) % 4 != 0) {
			appendString += "</section>";

		}
		counter = -1;

	});

	//finally, append  everything
	$(appendTo).append(appendString);

	//hide loading spinner
	$(".loading").hide();
}


/* *********************************************************
 * This function ןis responsible for the view and logic	    *
 * of the search by degree/course when a degree is selected *
 * *********************************************************
 */
function selectedDegreeTagClickListener(){
	$("#chosenDegreeTag").on("click", function() {
				//Hide explore arrows
					$("#browseRight , #browseLeft").css("visibility", "hidden");
			
				//Show main videos (popular + following)
				$("#exploreVideosWrapper").hide();
				$("#popularVideosWrapper, #followingVideosWrapper").show();

				if (canRemoveDegreeTag) {
					canRemoveDegreeTag = false;
					//1. empty containers - Tags holder and exploer title holder
					$("#tagsHolder , #selectionStringHolder").empty();

					//2. Set default explore title
					$("#selectionStringHolder").append("<h2>Explore</h2>");

					//3. Set arrow parameters to default
					startFrom = -numOfTagsToPresent;
					stopAt = -1;

					//4. disable arrows
					$("#arrowRight , #arrowLeft").removeClass('arrowEnabled').addClass('arrowDisabled');

					//6. populate explore tags holder with degree tags
					populateDegreeTagsToExplore();
				}
			});
}



/* *********************************************************
 * This function appends popular videos and videos 		   *
 * from users the current user follows.                    *
 * *********************************************************
 */
function showPopularAndFollowingVideos() {
	//Get the most pupular videos from the server and appent it to the DOM.
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/auxiliary/getTopRated',
		data : {
			"org" : userOrg
		},
		success : function(data) {
			if (data.status == 1) {
				//Hide loading spinner
				$(".loading").hide();

				jsonRef = data;
				//append data - first shuffle the videos and then append only first 8
				createTemplateAndAppendVideos((shuffle(data.res)).slice(0, 8), "#pvVideosHolder");
				$("#popularVideosWrapper").show();
			}
		},
		error : function(objRequest, errortype) {
			console.log("Cannot get popular videos Json");
		}
	});

	//Get latest videos from people the user follows -  from the server and appent it to the DOM.
	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/auxiliary/followedUsers',
		data : {
			email : userEmail,
			from : from,
			to : to
		},
		success : function(data) {
			if (data.status == 1 && !jQuery.isEmptyObject(data.res)) {
				//Hide loading spinner
				$(".loading").hide();

				jsonRef = data;
				//append data
				createTemplateAndAppendFollowingVideos(shuffle(data.res), "#fwVideosHolder");
				//show container
				$("#followingVideosWrapper").show();
			}

		},
		error : function(objRequest, errortype) {
			console.log("Cannot get followd users Json");
		}
	});
}

/* *************************************
 *  A function that shuffles an array  *
 *  ************************************
 */
function shuffle(arr) {
	for (var j,
	    x,
	    i = arr.length; i; j = Math.floor(Math.random() * i),
	x = arr[--i], arr[i] = arr[j], arr[j] =
	x);
	return arr;
};

/* *******************************************************
 *  A function that loads more data of followed people   *
 *  ******************************************************
 */

function loadMore() {
	from = to;
	to += 4;

	$.ajax({
		type : "POST",
		url : 'http://lecturus.herokuapp.com/auxiliary/followedUsers',
		data : {
			"email" : userEmail,
			from : from,
			to : to
		},
		success : function(data) {
			if (data.status == 1) {
				if (Object.keys(data.res).length < 4) {
					getMoreLectures = false;
				}
				jsonRef = data;
				createTemplateAndAppendFollowingVideos(data.res, "#fwVideosHolder");
				$("#loadingMoreMessage").hide();
			}
		},
		error : function(objRequest, errortype) {
			console.log("Cannot get Json");
		}
	});
}